#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on 12/2014
@author: Benoît Epinat
"""

import time
import os
import sys
import copy
import numpy as np
#import pyfits as pf
import astropy.io.fits as fits
from cap_mpfit import mpfit  # python 2.7 & 3!
#from camel.cap_mpfit import mpfit  # python 2.7 & 3!
#from mpfit import mpfit  # python 2.7...
#from matplotlib import pyplot as plt
from scipy import ndimage
from scipy import constants as ct

import argparse
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('CAMEL')


def dz_from_dv(dv, z):
    '''
    This function enables to compute the redshift interval corresponding to a given velocity interval
    
    Parameters
    ----------
    dv: float
        velocity interval in km/s
    z: float
        mean redshift of the source
    
    Returns
    -------
    dz: float
        corresponding redshift interval
    '''
    dz = dv * 1e3 / ct.c * (1 + z)
    return dz


def compute_rv(z, zcosmo=None):
    """This function computes the velocity from local and global redshifts

    Parameters
    ----------
    z: float (or numpy array of floats)
        local redshift (local velocity)
    zcosmo: float
        cosmological redshift (sytemic velocity)

    """

    if zcosmo is None:
        v = ((z + 1) ** 2 - 1) / ((z + 1) ** 2 + 1) * ct.c * 1e-3
    else:
        v = (z - zcosmo) / (1 + z) * ct.c * 1e-3
    return v


def compute_fwhm(dz, z=None, zcosmo=0):
    """This function computes the velocity variation

    Parameters
    ----------
    dz: float (or numpy array of floats)
        local redshift variation (local velocity dispersion)
    z: float (or numpy array of floats)
        local redshift (local velocity)
    zcosmo: float
        cosmological redshift (sytemic velocity)

    """

    if z is None:
        z = zcosmo
    dv = dz * (1 + zcosmo) / (1 + z) ** 2 * ct.c * 1e-3
    return dv


def readconfig(filename, config):
    """This function reads the configuration file and returns a configuration dictionary

    Parameters
    ----------
    filename: string
        name of the configuration file
    config: dictionary
        contains options with priority on the configuration file

    """

    conf = {}
    if filename != '':
        try:
            data = open(filename)
        except:
            logger.info('Camel is not able to open ' + filename)
            data = open(filename)
        count_extral = 0
        for raw in data:
            keyvalcom = raw.split('= ')
            key = keyvalcom[0].rstrip()   # on vire les espaces de fin de keyword
            value = keyvalcom[1].split('/ ')[0].rstrip('\t').rstrip()  # on vire les tabultation et les espaces de fin de keyword
            value = value.replace("'", '')
            value = value.replace('"', '')
            if value == '':
                value = None
            elif value.upper() == 'NONE':
                value = None
            elif value.upper() == 'FALSE':
                value = False
            elif value.upper() == 'TRUE':
                value = True
            #elif (value.count('.') == 1) & (value.replace('.', '').isdigit()):
                #value = float(value)
            elif (value.count('.') == 1):
                try:
                    value = float(value)
                except:
                    value = value
            #elif value.isdigit():
                #value = int(value)
            elif (value.count('.') == 0):
                try:
                    value = int(value)
                except:
                    value = value
            if (key == 'EXTRAL') & (value is not None):
                if count_extral == 0:
                    conf[key] = list()
                conf[key].append([value])
                count_extral += 1
            else:
                conf[key] = value

    # add input options to configuration dictionary + check needed keywords
    if config is not None:
        for key in config:
            if config[key] is not None:
                conf[key] = config[key]
            elif not(key in conf.keys()):
                conf[key] = None
                # XXX or give an error message but in that case, must test that the keywords have correct values
    # XXX more keywords? add default values?
    needed_keys = ['FITSFILE', 'OUTPUT', 'SKYFILE', 'HALPHA', 'NII6548', 'NII6583', 'SII6716', 'SII6731', 'OIII4959', 'OIII5007', 'HBETA', 'OII3729', 'OII3726', 'EXTRAL', 'COMMW', 'REDSHIFT', 'REDMIN', 'REDMAX', 'ZRMIN', 'ZRMAX', 'INITW', 'WMIN', 'WMAX', 'DFIT', 'DGCTNUM', 'MFIT', 'SCLIP', 'XYCLIP', 'NCLIP', 'SPSF', 'WSMOOTH', 'SSMOOTH', 'THRES', 'MEDIAN', 'FITSPSF', 'XMIN', 'YMIN', 'ZMIN', 'XMAX', 'YMAX', 'ZMAX']
    for key in needed_keys:
        if not(key in conf.keys()):
            conf[key] = None
    
    if 'OII' in conf.keys():
        conf['OII3729'] = conf['OII3726'] = conf.pop('OII')
    if conf['SPSF'] is None:
        conf['SPSF'] = 0.
    if conf['COMMW'] is None:
        conf['COMMW'] = False
            # XXX or give an error message but in that case, must test that the keywords have correct values
    if conf['ZRMIN'] is None:
        deltared = conf['REDMAX'] - conf['REDMIN']
        conf['ZRMIN'] = conf['REDMIN'] - deltared / 2.
    if conf['ZRMAX'] is None:
        deltared = conf['REDMAX'] - conf['REDMIN']
        conf['ZRMAX'] = conf['REDMAX'] + deltared / 2.
    
    return conf


def readdata_sdss(conf):
    """This function reads the input spectra (data and variance)

    Parameters
    ----------
    conf: configuration dictionary
        keyword to indicate if input fits has multiple extensions
    
    """
    
    hdul = fits.open(conf['FITSFILE'])
    hdr = hdul[0].header
    tab = hdul[1].data
    flux = tab['flux']
    llbda = tab['loglam']
    ivar = tab['ivar']
    sky = tab['sky']
    
    data = flux
    # for the variance, we can use either sky (assuming Poisson noise, i.e. dominated by photons) or 1/ivar, but ivar seems the best choice since it seems to include the variance due to the signal in the source, from the sky and possibly other sources of variance
    #var = sky
    var = 1 / ivar
    lbda = 10 ** llbda
    
    hdr['NAXIS1'] = len(llbda)
    hdr['CRPIX1'] = 1
    hdr['CRVAL1'] = llbda[0]
    hdr['CDELT1'] = llbda[1] - llbda[0]
    hdr['CUNIT1'] = 'log Angstroms'
    varhdr = hdr.copy()
    
    return data, hdr, var, varhdr


def readdata(conf, multi_ext=False):
    """This function reads the input spectra (data and variance)

    Parameters
    ----------
    conf: configuration dictionary
    multi_ext: bool
        keyword to indicate if input fits has multiple extensions
    
    """
    
    hdul = fits.open(conf['FITSFILE'])
    if multi_ext:
        data = hdul[1].data
        hdr = hdul[1].header
        if conf['SKYFILE'] == conf['FITSFILE']:
            var = hdul[2].data
            varhdr = hdul[2].header
        elif (not conf['SKYFILE']):
            var = None
            varhdr = ''
        else:
            hdul = fits.open(conf['SKYFILE'])
            var = hdul[0].data
            varhdr = hdul[0].header
    else:
        data = hdul[0].data
        hdr = hdul[0].header
        if conf['SKYFILE'] == conf['FITSFILE']:
            var = None
            varhdr = ''
        else:
            hdul = fits.open(conf['SKYFILE'])
            var = hdul[0].data
            varhdr = hdul[0].header
    return data, hdr, var, varhdr


def testdata(data, var):
    """This function tests the sizes of the variance data

    Parameters
    ----------
    data: ndarray
        input data spectrum
    var: ndarray
        input variance spectrum
    
    """
    if var is None:
        print('Variance is None')
        return
    if var.ndim != 1:
        print('Dimensions variance: ', var.ndim)
        sys.exit(2)
    else:
        if var.shape[0] != data.shape[0]:
            print(var.shape[0], data.shape[0])
            sys.exit(2)
    return


def writedata(data, hdr, filename):
    """This function writes the output data

    Parameters
    ----------
    data: ndarray
        data to be written
    hdr: fits header
        fits header to be written
    filename: string
        name of the output
    
    """
    hdu = fits.PrimaryHDU(data, hdr)
    hdulist = fits.HDUList(hdu)
    hdulist.writeto(filename, overwrite=True, output_verify='fix')
    return hdulist[0].header


def cutspectrum(data, hdr, conf):
    """This function cuts the cube according to the limits requested

    Parameters
    ----------
    data: array
        input data spectrum or variance
    hdr: fits header
        input data header
    conf: configuration dictionary

    """
    
    if conf['ZMIN'] is None:
        conf['ZMIN'] = 0
    if conf['ZMAX'] is None:
        conf['ZMAX'] = data.shape[0] - 1

    conf['ZMIN'] = np.max([conf['ZMIN'], 0], axis=0)
    conf['ZMAX'] = np.min([conf['ZMAX'], data.size - 1], axis=0)
    conf['ZMIN'] = np.min([conf['ZMIN'], conf['ZMAX']], axis=0)
    conf['ZMAX'] = np.max([conf['ZMAX'], conf['ZMIN']], axis=0)
    
    datacut = data[int(conf['ZMIN']):int(conf['ZMAX']) + 1]
    hdr['CRPIX1'] -= conf['ZMIN']
    return datacut, hdr


def spectralsmooth(data, hdr, fwhm):
    """This function performs a Gaussian spectral smoothing on the spectrum

    Parameters
    ----------
    data: array
        input spectrum
    hdr: fits header
        input data header
    fwhm: float
        full width half maximum of the 1D Gaussian kernel
    """
    
    data1 = np.zeros(data.shape, dtype='>f4')   # initialization of output spectrum
    sigma = fwhm / (2. * np.sqrt(2. * np.log(2.)))
    data1[:] = ndimage.gaussian_filter(data[:], sigma)
    hdr.append(('WSMOOTH', fwhm, 'FWHM in pixels of Gaussian spectral smoothing'))
    # XXX add Hanning, Gauss, ...
    return data1, hdr


def var_spectralsmooth(cube, hdr, fwhm):
    """This function performs a Gaussian spectral smoothing on the cube

    Parameters
    ----------
    cube: array
        input datacube
    hdr: fits header
        input data cube header
    fwhm: float
        full width half maximum of the 1D Gaussian kernel
    """
    
    cube1 = np.zeros(cube.shape, dtype='>f4')   # initialization of output cube
    sigma = fwhm / (2. * np.sqrt(2. * np.log(2.)))
    # For variance, we have to convolve with the square of the kernel.
    # In 1D, it is equivalent to convolve by a kernel of width sigma2 = sigma / np.sqrt(2), hence FWHM2 = FWHM/np.sqrt
    # and to renormalise the amplitude by multiplying by a factor fac = 1 / (2 * sigma) / np.sqrt(np.pi)
    sigma2 = sigma / np.sqrt(2)
    fac = 1 / (2 * sigma) / np.sqrt(np.pi)

    for x in range(cube.shape[2]):  # loop on the x range
        for y in range(cube.shape[1]):  # loop on the y range
            cube1[:, y, x] = ndimage.gaussian_filter(cube[:, y, x], sigma2)
    cube1 *= fac
    hdr.append(('WSMOOTH', fwhm, 'FWHM in pixels of Gaussian spectral smoothing'))
    # XXX add Hanning, Gauss, ...
    return cube1, hdr


class line:
    """This class is the basic for defining a line. A line is defined by its name, its wavelength and the reference line to which it is attached if the ratio has to be constrained.
    
    """
    def __init__(self, name, wave, ref=None, fit=False, low=0, up=None, th=None, index=None):
        self.index = index
        self.name = name
        self.wave = wave
        self.fit = fit
        if ref is None:
            ref = name
        self.ref = ref
        if ref == name:
            self.low = 1
            self.up = 1
            self.th = 1
        else:
            self.low = low
            self.up = up
            self.th = th


class lines:
    """This class enables to deal with lines.  A dictionary stored in lines will contain informations on each lines.
    
    """
    
    def append(self, line):
        self.lines[line.name] = line
        self.lines[line.name].index = self.index
        self.index += 1
        
    def __init__(self):
        self.index = 0
        self.lines = {}
        self.append(line('HALPHA', 6562.801, ref='HBETA', low=2.75, th=2.85))
        self.append(line('HBETA', 4861.363))
        self.append(line('HGAMMA', 4340.47, ref='HBETA', low=0.44, up=0.5, th=0.468))
        self.append(line('HDELTA', 4101.73, ref='HBETA', low=0.23, up=0.29, th=0.259))
        self.append(line('HEPS', 3970.07, ref='HBETA', low=0.13, up=0.19, th=0.159))
        self.append(line('NII6583', 6583.45, ref='NII6548', low=2.7, up=3.3, th=3.))
        #self.append(line('NII6583', 6583., ref='NII6548', low=2.7, up=3.3, th=3.))
        self.append(line('NII6548', 6548.05))
        self.append(line('SII6731', 6730.82))
        self.append(line('SII6716', 6716.44, ref='SII6731', low=0.45, up=1.45, th=1.))
        self.append(line('OIII5007', 5006.843, ref='OIII4959', low=2.7, up=3.3, th=3.))
        self.append(line('OIII4959', 4958.911))
        self.append(line('OIII4363', 4363.21, ref='OIII4959'))  # XXX low=, up=, th=
        self.append(line('OII3729', 3728.80, ref='OII3726', low=0.35, up=1.5, th=1.))
        self.append(line('OII3726', 3726.04))
        self.append(line('HEI4471', 4471.))
        self.append(line('HEI5876', 5876., ref='HEI4471', low=2.5, th=2.5))
        self.append(line('HEII4686', 4686.))
        self.append(line('OI6300', 6300.))
        self.append(line('NEIII3868', 3868.))

        #self['Ha'] =
        #self.names = ['Ha', 'Hb', 'Hga', 'Hd', 'Heps', 'NII6583', 'NII6548', 'SII6731', 'SII6716', 'OIII5007', 'OIII4959', 'OIII4363', 'OII3729', 'OII3726', 'HeI4471', 'HeI5876', 'HeII4686', 'OI6300', 'NeIII3868']
        #self.waves = [6562.8, 4861., 4340., 4101., 3968., 6583., 6548., 6731., 6717., 5007., 4959., 4363., 3729., 3726., 4471., 5876., 4686., 6300., 3868.]
        #self.ref = ['Hb', 'Hb', 'Hb', 'Hb', 'Hb', 'NII6548', 'NII6548', 'SII6731', 'SII6731', 'OIII4959', 'OIII4959', 'OIII4959', 'OII3726', 'OII3726', 'HeI4471', 'HeI4471', 'HeII4686', 'OI6300', 'NeIII3868']
        
        #lines={'Ha':6562.8,'Hb':4861.,'Hga':4340.,'Hd':4101.,'Heps':3968.,'NII6583':6583.,'NII6548':6548.,'SII6731':6731.,'SII6716':6717.,'OIII5007':5007.,'OIII4959':4959.,'OIII4363':4363.,'OII3729':3729.,'OII3726':3726.,'HeI4471':4471.,'HeI5876':5876.,'HeII4686':4686.,'OI6300':6300.,'NeIII3868':3868.}
        ##Initialisation of groups of lines
        #sgroup={'Ha':'Hb','Hb':'Hb','Hga':'Hb','Hd':'Hb','Heps':'Hb','NII6583':'NII6548','NII6548':'NII6548','SII6731':'SII6731','SII6716':'SII6731','OIII5007':'OIII4959','OIII4959':'OIII4959','OIII4363':'OIII4959','OII3729':'OII3726','OII3726':'OII3726','HeI4471':'HeI4471','HeI5876':'HeI4471','HeII4686':'HeII4686','OI6300':'OI6300','NeIII3868':'NeIII3868'}


def waveindgen(hdr):
    '''
    This function returns the wavelength index in angstroms from the header information and the conversion factor from header data to angstroms.
    '''
    
    cunit = hdr['CUNIT1']
    log = False
    if cunit.strip().lower() in {'microns', 'micron', 'micrometers', 'mum'}:
        lconvfac = 1e4
    if cunit.strip().lower() in {'angstroms', 'angstrom'}:
        lconvfac = 1
    if cunit.strip().lower() in {'nanometers', 'nm'}:
        lconvfac = 1e1
    if cunit.strip().lower() in {'centimeters', 'cm'}:
        lconvfac = 1e8
    if cunit.strip().lower() in {'log angstroms'}:
        lconvfac = 1
        log = True
    if 'CD1_1' in hdr.keys():
        sclp = np.ones(hdr['NAXIS1']) * hdr['CD1_1']
    elif 'CDELT1' in hdr.keys():
        sclp = np.ones(hdr['NAXIS1']) * hdr['CDELT1']
    if log:
        lwave = (sclp * (np.arange(hdr['NAXIS1']) - hdr['CRPIX1'] + 1) + hdr['CRVAL1'])
        wave = 10 ** (lwave) * lconvfac
        sclp = np.gradient(wave)
    else:
        wave = lconvfac * (sclp * (np.arange(hdr['NAXIS1']) - hdr['CRPIX1'] + 1) + hdr['CRVAL1'])
    return wave, lconvfac, sclp


def elspectrum(p, wave=None, lines=None, psf=None, degc=None, zind=None, sigind=None, intind=None, ratioind=None, contind=None):
    wavel = wave.reshape(wave.size, 1)
    deg = np.arange(degc + 1)
    coefs = p[contind]
    continuum = (coefs * (wavel - wave[0]) ** deg).sum(axis=1)
    wavel1 = (lines * (p[zind] + 1))
    dlines = (p[sigind] * lines)
    coefs = (p[intind] * p[ratioind])
    slines = (coefs * np.exp(-0.5 * (wavel - wavel1) ** 2 / (dlines ** 2 + psf ** 2))).sum(axis=1)
    
    #wavel = wave.reshape(wave.size, 1)
    #deg = np.arange(degc + 1)
    #coefs = p[lines.size * 4:lines.size * 4 + degc + 1]
    #continuum = (coefs * (wavel - wave[0]) ** deg).sum(axis=1)
    #wavel1 = (lines * (p[0:lines.size] + 1))
    #dlines = (p[lines.size:lines.size * 2] * lines)
    #coefs = (p[lines.size * 2:lines.size * 3] * p[lines.size * 3:lines.size * 4])
    #slines = (coefs * np.exp( -0.5 * (wavel - wavel1) ** 2 / (dlines ** 2 + psf ** 2))).sum(axis=1)
    
    #wavel = wave
    #deg = np.arange(degc + 1).reshape(degc + 1, 1)
    #coefs = p[lines.size * 4:lines.size * 4 + degc + 1].reshape(degc + 1, 1)
    #continuum = (coefs * (wavel - wave[0]) ** deg).sum(axis=0)
    #wavel1 = (lines * (p[0:lines.size] + 1)).reshape(lines.size, 1)
    #dlines = (p[lines.size:lines.size * 2] * lines).reshape(lines.size, 1)
    #coefs = (p[lines.size * 2:lines.size * 3] * p[lines.size * 3:lines.size * 4]).reshape(lines.size, 1)
    #slines = (coefs * np.exp( -0.5 * (wavel - wavel1) ** 2 / (dlines ** 2 + psf ** 2))).sum(axis=0)

    #wavel = wave.reshape(1, wave.size)
    #deg = np.arange(degc + 1).reshape(degc + 1, 1)
    #coefs = p[lines.size * 4:lines.size * 4 + degc + 1].reshape(degc + 1, 1)
    #continuum = (coefs * (wavel - wave[0]) ** deg).sum(axis=0)
    #wavel1 = (lines * (p[0:lines.size] + 1)).reshape(lines.size, 1)
    #dlines = (p[lines.size:lines.size * 2] * lines).reshape(lines.size, 1)
    #coefs = (p[lines.size * 2:lines.size * 3] * p[lines.size * 3:lines.size * 4]).reshape(lines.size, 1)
    #slines = (coefs * np.exp( -0.5 * (wavel - wavel1) ** 2 / (dlines ** 2 + psf ** 2))).sum(axis=0)
    return (continuum + slines)


def myelspectrum(p, fjac=None, wave=None, spectrum=None, err=None, lines=None, psf=None, degc=None, zind=None, sigind=None, intind=None, ratioind=None, contind=None):
    #t1=time.time()
    model = elspectrum(p, wave=wave, lines=lines, psf=psf, degc=degc, zind=zind, sigind=sigind, intind=intind, ratioind=ratioind, contind=contind)
    status = 0
    #t2=time.time()
    #print(t2-t1)
    return [status, (spectrum - model) / err]


def build_parinfo_lines(els, linename, conf, free_ratio, comind, multi_comp=1):
    """
    els: emission lines (not organised)
    linename: name of lines organised
    conf: configuration parameters
    free_ratio:
    comind: Index of the reference line for redshift and eventually dispersion
    """
    zcosmo = conf['REDSHIFT']
    zmax = conf['REDMAX']
    zmin = conf['REDMIN']
    
    parbase = {'value': 0., 'fixed': 1, 'limited': [0, 0], 'limits': [0., 0.], 'tied': '', 'mpmaxstep': 0, 'mpminstep': 0, 'parname': ''}
    parinfo = []
    
    #Creation of parameter constraints for all lines
    for i in range(els.index * 4):
        parinfo.append(copy.deepcopy(parbase))
        
    #We set the redshift as free parameter for the reference line
    parinfo[comind]['fixed'] = 0
    
    #When common dispersion, we set the dispersion as free parameter for the reference line
    if conf['COMMW']:
        parinfo[els.index + comind]['fixed'] = 0

    for i in range(els.index):
        name = linename[i]
        j = els.lines[els.lines[name].ref].index
        
        #Redshift
        parinfo[i]['limited'] = [1, 1]
        parinfo[i]['limits'] = [zmin, zmax]
        #Redshift is common for all lines: tied to the first free line
        if (i != comind) & (els.lines[name].fit is True):
            parinfo[i]['tied'] = 'p[%i]' % (comind + multi_comp * len(parinfo))
        
        #Dispersion
        parinfo[els.index + i]['limited'] = [1, 1]
        parinfo[els.index + i]['limits'] = [(conf['WMIN'] * 1e3 / ct.c + zcosmo) / (1 - conf['WMIN'] * 1e3 / ct.c) - zcosmo, (conf['WMAX'] * 1e3 / ct.c + zcosmo) / (1 - conf['WMAX'] * 1e3 / ct.c) - zcosmo]  # in redshift unit
        parinfo[els.index + i]['value'] = (conf['INITW'] * 1e3 / ct.c + zcosmo) / (1 - conf['INITW'] * 1e3 / ct.c) - zcosmo
        
        #When dispersion is common for all lines, widths are tied to the first free line (the first free width is free)
        if (conf['COMMW']) & (i != comind) & (els.lines[name].fit is True):
            parinfo[els.index + i]['tied'] = 'p[%i]' % (els.index + comind + multi_comp * len(parinfo))
        
        #When dispersion is independant for all lines (common by groups), widths are tied to the reference line and the ref width is free
        if (not conf['COMMW']) & (i != j):
            parinfo[els.index + i]['tied'] = 'p[%i]' % (els.index + j + multi_comp * len(parinfo))
            if els.lines[name].fit:
                parinfo[els.index + j]['fixed'] = 0  # the width of the reference line is free
        if (not conf['COMMW']) & (i == j) & (els.lines[name].fit is True):
            parinfo[els.index + i]['fixed'] = 0  # the width of the reference line is free
        
        #XXX When dispersion is completely independant, widths are not tied and are free
        #if (not conf['COMMW']):
            #if els.lines[name].fit: parinfo[els.index + i]['fixed'] = 0 # the width of the reference line is free
        
        #Intensity
        parinfo[2 * els.index + i]['limited'][0] = 1
        parinfo[2 * els.index + i]['limits'][0] = 0.
        
        #When line ratio is free: all ratios are fixed (= 1) and lines intensities are not tied and free parameters
        if free_ratio:
            parinfo[3 * els.index + i]['value'] = 1
            if els.lines[name].fit is True:
                parinfo[2 * els.index + i]['fixed'] = 0
        
        #When line ratio is constrained: line intensities are tied and ratios are free, except for the reference line (the ref intensity is free, the ref ratio is fixed = 1)
        if (not free_ratio) & (i != j) & (els.lines[name].fit is True):
            parinfo[2 * els.index + i]['tied'] = 'p[%i]' % (2 * els.index + j + multi_comp * len(parinfo))  # line intensity is tied to reference
            #Ratio limits
            parinfo[3 * els.index + i]['limited'][0] = 1
            if els.lines[name].low is not None:
                parinfo[3 * els.index + i]['limits'][0] = els.lines[name].low
            if els.lines[name].up is not None:
                parinfo[3 * els.index + i]['limited'][1] = 1
                parinfo[3 * els.index + i]['limits'][1] = els.lines[name].up
            if els.lines[name].th is not None:
                parinfo[3 * els.index + i]['value'] = els.lines[name].th
                #params[3 * els.index + i] = els.lines[name].th
            else:
                parinfo[3 * els.index + i]['value'] = 1
                #params[3 * els.index + i] = 1
            
            #Free parameters
            if els.lines[name].fit is True:
                parinfo[2 * els.index + j]['fixed'] = 0  # the intensity of the reference line is free
                parinfo[3 * els.index + i]['fixed'] = 0  # line ratio is free
                #XXX When reference line is not fitted, we have to find a solution if several lines of a group are fitted
            
        if (not free_ratio) & (i == j) & (els.lines[name].fit is True):
            #When line is reference line, the ratio is fixed equal to 1
            parinfo[3 * els.index + i]['limited'] = [0, 0]
            parinfo[3 * els.index + i]['value'] = 1
            #params[3 * els.index + i] = 1
            parinfo[2 * els.index + i]['fixed'] = 0
                
        #XXX When line ratio is fixed: all ratios are fixed equal to theoretical values and intensities are tied (the ref intensity is free)
        #if (not free_ratio) & (i!=j):
            #parinfo[2 * els.index + i]['tied'] = 'p[%i]'%(2 * els.index + j + multi_comp * len(parinfo))
            #parinfo[3 * els.index + i]['limited'][0] = 1
            #if els.lines[name].low is not None:
                #parinfo[3 * els.index + i]['limits'][0] = els.lines[name].low
            #if els.lines[name].up is not None:
                #parinfo[3 * els.index + i]['limited'][1] = 1
                #parinfo[3 * els.index + i]['limits'][1] = els.lines[name].up
            #if els.lines[name].th is not None:
                #parinfo[3 * els.index + i]['value'] = params[3 * els.index + i] = els.lines[name].th
            #else:
                #params[3 * els.index + i] = 1
            #if els.lines[name].fit is True:
                #parinfo[2 * els.index + j]['fixed'] = 0 # the intensity of the reference line is free
            
        #if (not free_ratio) & (i == j) & (els.lines[name].fit is True):
            ##When line is reference line, the ratio is fixed equal to 1
            #parinfo[3 * els.index + i]['limited'] = [0, 0]
            #parinfo[3 * els.index + i]['value'] = params[3 * els.index + i] = 1
            #parinfo[2 * els.index + i]['fixed'] = 0
    return parinfo


def buildmaps(conf, data, hdr, var=None, plot=False, debug=False, free_ratio=False, multi_comp=1, factor=10, ftol=1e-7, gtol=1e-7, xtol=1e-7, maxiter=200):
    """
    """

    #Initialisation of lines
    els = lines()
    for lline in els.lines:
        #check the line name is set in the configuration file
        if lline in conf.keys():
            els.lines[lline].fit = conf[lline]

    if conf['EXTRAL'] is not None:
        for i in np.arange(np.shape(conf['EXTRAL'])[0]):
            if np.shape(conf['EXTRAL'][i])[0] == 1:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True))
            if np.shape(conf['EXTRAL'][i])[0] == 2:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1]))
            if np.shape(conf['EXTRAL'][i])[0] == 3:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1], th=float(conf['EXTRAL'][i][2])))
            if np.shape(conf['EXTRAL'][i])[0] == 4:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1], th=float(conf['EXTRAL'][i][2]), low=float(conf['EXTRAL'][i][3])))
            if np.shape(conf['EXTRAL'][i])[0] == 5:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1], th=float(conf['EXTRAL'][i][2]), low=float(conf['EXTRAL'][i][3]), up=float(conf['EXTRAL'][i][4])))
    
    #Construction of wavelength index
    wave0, lconvfac, sclp0 = waveindgen(hdr)  # wavelength array in Angstrom and conversion factor

    #Condition on the wavelength to be within the redshift cuts
    #XXX try zmin > zmax?
    zmax = conf['ZRMAX']
    zmin = conf['ZRMIN']
    
    argsorted = np.argsort(np.array([els.lines[i].index for i in els.lines]))
    lineindex = np.array([els.lines[i].index for i in els.lines])[argsorted]
    linefit = np.array([els.lines[i].fit for i in els.lines])[argsorted]
    linewave = np.array([els.lines[i].wave for i in els.lines])[argsorted]
    linename = np.array([els.lines[i].name for i in els.lines])[argsorted]
    indok = np.zeros(0, dtype=int)
    firstind = np.zeros(linefit.size, dtype=int)
    lastind = np.zeros(linefit.size, dtype=int)
    firstind0 = np.zeros(linefit.size, dtype=int)
    lastind0 = np.zeros(linefit.size, dtype=int)
    for i in range(linefit.size):
        if (not linefit[i]):
            continue
        linf = np.max([(zmin + 1) * linewave[i], np.min(wave0)])
        lsup = np.min([(zmax + 1) * linewave[i], np.max(wave0)])
        condo = np.where((wave0 >= linf) & (wave0 <= lsup))
        if np.size(condo) != 0:
            firstind0[i] = condo[0][0]
            lastind0[i] = condo[0][np.size(condo) - 1]
            indok = np.append(indok, condo)
    indok = np.unique(indok)
    wave = wave0[indok]
    sclp = sclp0[indok]
    
    for i in range(linefit.size):
        if (not linefit[i]):
            continue
        linf = np.max([(zmin + 1) * linewave[i], np.min(wave0)])
        lsup = np.min([(zmax + 1) * linewave[i], np.max(wave0)])
        condo = np.where((wave >= linf) & (wave <= lsup))
        if np.size(condo) != 0:
            firstind[i] = condo[0][0]
            lastind[i] = condo[0][np.size(condo) - 1]

    #Weights
    if var is None:
        var = np.ones(data.shape, dtype='>f4')
    weight = ((1. / np.sqrt(var)) / np.sum(1. / np.sqrt(var), axis=0)) * var.shape[0]
    
    #XXX try zmin > zmax?
    zcosmo = conf['REDSHIFT']
    zmax = conf['REDMAX']
    zmin = conf['REDMIN']
    
    #Redshift step
    zstep = dz_from_dv(conf['DFIT'], zcosmo)
    #zstep = (conf['DFIT'] * 1e3 / ct.c + zcosmo) / (1 - conf['DFIT'] * 1e3 / ct.c) - zcosmo

    #if zstep >= (zmax - zmin) / 2: zstep = (zmax - zmin) / 2.
    zmean = (zmax + zmin) / 2.
    zzmax = zmean + np.floor((zmax - zmean) / zstep + 1) * zstep
    zzmin = zmean + np.ceil((zmin - zmean) / zstep) * zstep

    #Parameters contraints and informations: each line has z, sigma, intensity and ratio constraint

    #Index of the reference line for redshift and eventually dispersion
    comind = np.min(lineindex[np.where(linefit)])
    
    #parinfo = build_parinfo_lines(els, linename, conf, free_ratio, comind)
    parinfo = []
    zind = []
    sigind = []
    intind = []
    ratioind = []
    #Lines parameters
    linewaves = np.array(())
    components = np.zeros(0, dtype='i')
    for comp in range(multi_comp):
        zind.extend(range(len(parinfo) + 0 * len(linewave), len(parinfo) + 1 * len(linewave)))
        sigind.extend(range(len(parinfo) + 1 * len(linewave), len(parinfo) + 2 * len(linewave)))
        intind.extend(range(len(parinfo) + 2 * len(linewave), len(parinfo) + 3 * len(linewave)))
        ratioind.extend(range(len(parinfo) + 3 * len(linewave), len(parinfo) + 4 * len(linewave)))
        parinfom = build_parinfo_lines(els, linename, conf, free_ratio, comind, multi_comp=comp)
        parinfo.extend(parinfom)
        linewaves = np.append(linewaves, linewave)
        components = np.append(components, np.ones(len(parinfom), dtype='i') * (comp + 1))

    #Continuum parameters
    parbase = {'value': 0., 'fixed': 0, 'limited': [0, 0], 'limits': [0., 0.], 'tied': '', 'mpmaxstep': 0, 'mpminstep': 0, 'parname': ''}
    if conf['DGCTNUM'] == -1:  # case without continuum
        conf['DGCTNUM'] = 0
        parbase['fixed'] = 1
        contfixed = True
    else:
        contfixed = False
    contind = [i for i in range(len(parinfo), len(parinfo) + conf['DGCTNUM'] + 1)]
    for i in range(conf['DGCTNUM'] + 1):
        parinfo.append(copy.deepcopy(parbase))
        components = np.append(components, -1)
    params = np.zeros(len(parinfo), dtype='>f4')
    for i in range(len(parinfo)):
        params[i] = parinfo[i]['value']
    
    logger.info(' Buildmaps: initial parameters % s' % (str(params)))

    #Output initialisation to be filled
    paramsdata = np.zeros(len(params), dtype='>f4')
    perrordata = np.zeros(len(params), dtype='>f4')
    status = 0
    dof = 0
    fnorm = 0
    moddata = np.zeros(data.shape, dtype='>f4')
    
    #List of extra parameters
    #xxx Ajuster wave et spectrum pour ne garder que ce qui nous intéresse?
    fa = {'wave': wave, 'spectrum': None, 'err': None, 'lines': linewaves, 'psf': conf['SPSF'], 'degc': conf['DGCTNUM'], 'zind': zind, 'sigind': sigind, 'intind': intind, 'ratioind': ratioind, 'contind': contind}

    #
    tii = time.time()
    tt = 0
    
    normsp = 10. ** np.floor(np.log10(np.nanstd(data[indok])))
    
    fa['spectrum'] = data[indok] / normsp  # Spectrum
    fa['err'] = np.sqrt(var[indok]) / normsp  # Error
    if (np.min(fa['spectrum']) == np.max(fa['spectrum'])) & (np.min(fa['spectrum']) == 0):
        return  # No need for fitting
    
    minfnorm = np.infty
    
    std = np.std(fa['spectrum'])
    
    #Parameters initialisation
    p = np.copy(params)
    
    #Line intensity initialisation
    for i in intind:
        if parinfo[i]['fixed'] == 0:
            p[i] = 5 * std * 10 ** (- components[i] + 1.)

    #Initialisation of continuum (degree 0)
    if not contfixed:
        p[contind[0]] = np.median(fa['spectrum'])
    for z in np.arange(zzmin, zzmax, zstep):
        #print(z)
        p[zind] = z
        pstart = np.copy(p)
        ti = time.time()
        fit0 = mpfit(myelspectrum, xall=pstart, functkw=fa, quiet=True, parinfo=parinfo, ftol=ftol, xtol=xtol, gtol=gtol, maxiter=maxiter, factor=factor)
        tf = time.time()
        tt += tf - ti
        #print(fit0.status)
        if fit0.status == 0:
            continue
        if fit0.status == -16:
            continue
        if fit0.fnorm <= minfnorm:
            minfnorm = fit0.fnorm
            fit = copy.copy(fit0)
        #print(z,tf - ti)
    if minfnorm == np.infty:
        print('infinity chi2!')
        return
    #logger.debug(fit.params[2 * els.index + 1 + 12], parinfo[2 * els.index + 1 + 12]['limits'], fit.params[2 * els.index + 1 + 13])
    ####################################
    # sort of the components based on their flux. In a second pass, we could start from the output parameters and set constraints on e.g. sigma, intensity (e.g. int[comp2] < int[comp1]), etc.)
    # tie intensities to impose one lower than the other?
    if multi_comp > 1:
        ftot = np.zeros(multi_comp)
        for comp in range(multi_comp):
            ind = (components == (comp + 1))
            ftot[comp] = (fit.params[np.array(intind)[ind[intind]]] * fit.params[np.array(ratioind)[ind[ratioind]]] * fit.params[np.array(sigind)[ind[sigind]]]).sum()
        sortind = ftot[::-1].argsort() + 1
        paramssort = np.copy(fit.params)
        perrorsort = np.copy(fit.perror)
        for comp in range(multi_comp):
            paramssort[components == (comp + 1)] = fit.params[components == sortind[comp]]
            perrorsort[components == (comp + 1)] = fit.perror[components == sortind[comp]]
        fit.params = np.copy(paramssort)
        fit.perror = np.copy(perrorsort)
    paramsdata[:] = fit.params[:]
    perrordata[:] = fit.perror[:]
    fnorm = fit.fnorm
    dof = fit.dof
    status = fit.status
    moddata[indok] = elspectrum(fit.params, wave=wave, lines=linewaves, psf=conf['SPSF'], degc=conf['DGCTNUM'], zind=zind, sigind=sigind, intind=intind, ratioind=ratioind, contind=contind)
    
    tff = time.time()
    logger.debug('Execution time: %3.1f' % (tff - tii))
    #print(tt)

    #Creating outputs
    moddata *= normsp
    #Error normalisation by reduced chi2
    perrordata = perrordata * np.sqrt(fnorm / dof)

    #Continuum
    #if we want the coefficient, we must multiply them by factor + give lambda[0]
    contdata = np.zeros(data.shape, dtype='>f4')
    econtdata = np.zeros(data.shape, dtype='>f4')
    for i in range(conf['DGCTNUM'] + 1):
        contdata += paramsdata[contind[i]] * (wave0 - wave[0]) ** i
        econtdata += perrordata[contind[i]] * (wave0 - wave[0]) ** i
    contdata *= normsp
    econtdata *= normsp
    
    #Residual data
    #residual
    residualdata = np.zeros(data.shape, dtype='>f4')
    residualdata[indok] = data[indok] - moddata[indok]
    #dispcont
    dispcont = np.std(data[indok] - moddata[indok], axis=0)
    
    #Chi2
    chi2 = fnorm / dof
    
    wavemaps = np.zeros(els.index * multi_comp, dtype='>f4')
    ewavemaps = np.zeros(els.index * multi_comp, dtype='>f4')
    wavedispmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    ewavedispmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    dispmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    edispmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    intmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    eintmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    fluxmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    efluxmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    snrmaps = np.zeros(els.index * multi_comp, dtype='>f4')
    zmap = np.zeros(els.index * multi_comp, dtype='>f4')
    ezmap = np.zeros(els.index * multi_comp, dtype='>f4')
    rvmap = np.zeros(els.index * multi_comp, dtype='>f4')
    ervmap = np.zeros(els.index * multi_comp, dtype='>f4')
    
    for comp in range(multi_comp):
        for i in range(els.index):
            if (not linefit[i]):
                continue
            name = linename[i]
            j = els.lines[els.lines[name].ref].index
            
            #Velocity field
            zmap[comp * els.index + i] = paramsdata[zind[comp * els.index + i]]
            ezmap[comp * els.index + i] = perrordata[zind[comp * els.index + i]]
            rvmap[comp * els.index + i] = compute_rv(zmap[comp * els.index + i], zcosmo=zcosmo)
            ervmap[comp * els.index + i] = compute_fwhm(ezmap[comp * els.index + i], z=zmap[comp * els.index + i], zcosmo=zcosmo)
            wavemaps[comp * els.index + i] = (zmap[comp * els.index + i] + 1) * linewave[i]
            ewavemaps[comp * els.index + i] = ezmap[comp * els.index + i] * linewave[i]
            
            #Velocity dispersion
            dzmap = paramsdata[sigind[comp * els.index + i]]
            edzmap = perrordata[sigind[comp * els.index + i]]
            wavedispmaps[comp * els.index + i] = dzmap * linewave[i]
            ewavedispmaps[comp * els.index + i] = edzmap * linewave[i]
            dispmaps[comp * els.index + i] = compute_fwhm(dzmap, z=zmap[comp * els.index + i], zcosmo=zcosmo)
            edispmaps[comp * els.index + i] = compute_fwhm(edzmap, z=zmap[comp * els.index + i], zcosmo=zcosmo)
            
            #Flux
            intmaps[comp * els.index + i] = paramsdata[intind[comp * els.index + i]] * paramsdata[ratioind[comp * els.index + i]] * normsp
            #fluxmaps[comp * els.index + i] = np.sum(intmaps[comp * els.index + i] * np.exp(-0.5 * (wave.reshape(wave.size, 1, 1) - linewave[i] * (1 + zmap[comp * els.index + i])) ** 2 / ((dzmap * linewave[i]) ** 2 + conf['SPSF'] ** 2)) * sclp * lconvfac, axis=0)
            fluxmaps[comp * els.index + i] = np.sum(intmaps[comp * els.index + i] * np.exp(-0.5 * (wave.reshape(wave.size, 1, 1) - linewave[i] * (1 + zmap[comp * els.index + i])) ** 2 / ((dzmap * linewave[i]) ** 2 + conf['SPSF'] ** 2)) * sclp.reshape(wave.size, 1, 1) * lconvfac, axis=0)
            #snrmaps[comp * els.index + i] = np.sum(weight[firstind0[i]:lastind0[i]] * (moddata[firstind0[i]:lastind0[i]] - contdata[firstind0[i]:lastind0[i]]), axis=0) * sclp * lconvfac / (np.sqrt(np.pi * 2) * np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2) * np.sqrt(np.sum(weight[firstind0[i]:lastind0[i]] * (data[firstind0[i]:lastind0[i]] - moddata[firstind0[i]:lastind0[i]]) ** 2, axis=0) / np.sum(weight[firstind0[i]:lastind0[i]], axis=0))) * (lastind0[i] - firstind0[i]) / np.sum(weight[firstind0[i]:lastind0[i]], axis=0)
            snrmaps[comp * els.index + i] = np.sum(weight[firstind0[i]:lastind0[i]] * (moddata[firstind0[i]:lastind0[i]] - contdata[firstind0[i]:lastind0[i]]) * sclp0[firstind0[i]:lastind0[i]], axis=0) * lconvfac / (np.sqrt(np.pi * 2) * np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2) * np.sqrt(np.sum(weight[firstind0[i]:lastind0[i]] * (data[firstind0[i]:lastind0[i]] - moddata[firstind0[i]:lastind0[i]]) ** 2, axis=0) / np.sum(weight[firstind0[i]:lastind0[i]], axis=0))) * (lastind0[i] - firstind0[i]) / np.sum(weight[firstind0[i]:lastind0[i]], axis=0)
            if free_ratio:
                eintmaps[comp * els.index + i] = perrordata[intind[comp * els.index + i]] * normsp
            elif i != j:
                eintmaps[comp * els.index + i] = np.sqrt((paramsdata[intind[comp * els.index + i]] * perrordata[ratioind[comp * els.index + i]]) ** 2 + (perrordata[intind[comp * els.index + j]] * paramsdata[ratioind[comp * els.index + i]]) ** 2) * normsp
            else:
                eintmaps[comp * els.index + i] = perrordata[intind[comp * els.index + i]] * normsp
            
            efluxmaps[comp * els.index + i] = np.sqrt(np.pi * 2) * np.sqrt((eintmaps[comp * els.index + i] * np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2)) ** 2 + (linewave[i] * dzmap / np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2) * intmaps[comp * els.index + i] * edzmap) ** 2)
            
        # This definition suffers inclusion of dz and z, especially when multiple components are fitted
        #snr = np.sum(weight[indok] * (moddata[indok] - contdata[indok]), axis=0) * sclp * lconvfac * np.size(indok) / (np.sum(weight[indok], axis=0)) / (np.sqrt(np.pi * 2) * np.sqrt((dzmap * np.median(wave) / (1 + zmap[comind])) ** 2 + conf['SPSF'] ** 2) * np.sqrt(np.sum(weight[indok] * (data[indok] - moddata[indok]) ** 2, axis=0) / np.sum(weight[indok], axis=0)))
        snr = np.sum(weight[indok] * (moddata[indok] - contdata[indok]) * sclp0[indok], axis=0) * lconvfac * np.size(indok) / (np.sum(weight[indok], axis=0)) / (np.sqrt(np.pi * 2) * np.sqrt((dzmap * np.median(wave) / (1 + zmap[comind])) ** 2 + conf['SPSF'] ** 2) * np.sqrt(np.sum(weight[indok] * (data[indok] - moddata[indok]) ** 2, axis=0) / np.sum(weight[indok], axis=0)))

    #Output writing
    suff = ''
    if conf['COMMW']:
        suff = '_common'
    else:
        suff = '_indep'
    #datas
    moddata2 = moddata * 0
    moddata2[indok] = moddata[indok] - contdata[indok]
    hh = writedata(residualdata, hdr, conf['OUTPUT'] + suff + '_residualdata.fits')
    hh = writedata(moddata, hdr, conf['OUTPUT'] + suff + '_moddata.fits')
    hh = writedata(moddata2, hdr, conf['OUTPUT'] + suff + '_linesdata.fits')
    hh = writedata(contdata, hdr, conf['OUTPUT'] + suff + '_contdata.fits')
    hh = writedata(econtdata, hdr, conf['OUTPUT'] + suff + '_econtdata.fits')
    
    f = open(conf['OUTPUT'] + suff + '_fit_result.txt', 'w')
    stdout = sys.stdout
    sys.stdout = f
    
    print(suff)
    print('--------------------------')
    #paramsdata
    #perrordata
    print('Status: %5i' % (status))
    print('Fnorm: %5.2f' % (fnorm))
    print('DOF: %5i' % (dof))
    print('Chi2: %5.2f' % (chi2))
    print('SNR: %5.2f' % (snr))
    print('Disp_cont: %5.2f' % (dispcont))
    print('Sigma_LSF : %5.2f' %(conf['SPSF']))
    
    print('--------------------------')
    
    dtype0 = np.dtype({'names': ['status', 'fnorm', 'dof', 'chi2r', 'snr', 'dispcont', 'Sigma_LSF'], 'formats': [np.int16, np.float32, np.int16, np.float32, np.float32, np.float32, np.float32]})
    res0 = np.ndarray(1, dtype=dtype0)
    res0['status'] = status
    res0['fnorm'] = fnorm
    res0['dof'] = dof
    res0['chi2r'] = chi2
    res0['snr'] = snr
    res0['dispcont'] = dispcont
    res0['Sigma_LSF'] = conf['SPSF']
    
    #res0 = np.array((status, fnorm, dof, chi2, snr, dispcont), dtype=dtype0)
    
    nfit = np.size(np.where(linefit))
    dtypec = np.dtype({'names': ['comp', 'line', 'z', 'dz', 'v', 'dv', 'sigv', 'dsigv', 'lbda', 'dlbda', 'sigl', 'dsigl', 'int', 'dint', 'flux', 'dflux', 'snr'], 'formats': [np.int16, 'a15', np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32, np.float32]})
    resc = np.ndarray(multi_comp * nfit, dtype=dtypec)
    for comp in range(multi_comp):
        count = 0
        print('Component %5i' % (comp + 1))
        print('Redshift: %5.5f' % (zmap[comp * els.index + comind]))
        print('Redshift error: %5.5f' % (ezmap[comp * els.index + comind]))
        print('Velocity: %5.2f' % (rvmap[comp * els.index + comind]))
        print('Velocity error: %5.2f' % (ervmap[comp * els.index + comind]))
        
        if conf['COMMW']:
            print('Dispersion: %5.2f' % (dispmaps[comp * els.index + comind]))
            print('Dispersion error: %5.2f' % (edispmaps[comp * els.index + comind]))
        
        print('--------------------------')
        for i in range(els.index):
            if (not linefit[i]):
                continue
            resc[comp * nfit + count]['comp'] = comp + 1
            resc[comp * nfit + count]['line'] = linename[i]
            resc[comp * nfit + count]['z'] = zmap[comp * els.index + comind]
            resc[comp * nfit + count]['dz'] = ezmap[comp * els.index + comind]
            resc[comp * nfit + count]['v'] = rvmap[comp * els.index + comind]
            resc[comp * nfit + count]['dv'] = ervmap[comp * els.index + comind]
            if not(conf['COMMW']):
                resc[comp * nfit + count]['sigv'] = dispmaps[comp * els.index + i]
                resc[comp * nfit + count]['dsigv'] = edispmaps[comp * els.index + i]
            else:
                resc[comp * nfit + count]['sigv'] = dispmaps[comp * els.index + comind]
                resc[comp * nfit + count]['dsigv'] = edispmaps[comp * els.index + comind]
            resc[comp * nfit + count]['lbda'] = wavemaps[comp * els.index + i]
            resc[comp * nfit + count]['dlbda'] = ewavemaps[comp * els.index + i]
            resc[comp * nfit + count]['sigl'] = wavedispmaps[comp * els.index + i]
            resc[comp * nfit + count]['dsigl'] = ewavedispmaps[comp * els.index + i]
            resc[comp * nfit + count]['int'] = intmaps[comp * els.index + i]
            resc[comp * nfit + count]['dint'] = eintmaps[comp * els.index + i]
            resc[comp * nfit + count]['flux'] = fluxmaps[comp * els.index + i]
            resc[comp * nfit + count]['dflux'] = efluxmaps[comp * els.index + i]
            resc[comp * nfit + count]['snr'] = snrmaps[comp * els.index + i]
            
            print(linename[i])
            if not(conf['COMMW']):
                print('Dispersion: %5.2f' % (dispmaps[comp * els.index + i]))
                print('Dispersion error: %5.2f' % (edispmaps[comp * els.index + i]))
            print('Wave: %5.2f' % (wavemaps[comp * els.index + i]))
            print('Wave error: %5.2f' % (ewavemaps[comp * els.index + i]))
            print('Wave dispersion: %5.2f' % (wavedispmaps[comp * els.index + i]))
            print('Wave dispersion error: %5.2f' % (ewavedispmaps[comp * els.index + i]))
            print('Intensity: %5.2f' % (intmaps[comp * els.index + i]))
            print('Intensity error: %5.2f' % (eintmaps[comp * els.index + i]))
            print('Flux: %5.2f' % (fluxmaps[comp * els.index + i]))
            print('Flux error: %5.2f' % (efluxmaps[comp * els.index + i]))
            print('SNR: %5.2f' % (snrmaps[comp * els.index + i]))
            print('--------------------------')
            count += 1
    
    print('--------------------------')
    sys.stdout = stdout
    
    new_hdul = fits.HDUList()
    new_hdul.append(fits.BinTableHDU(data=res0))
    new_hdul.append(fits.BinTableHDU(data=resc))
    new_hdul.writeto(conf['OUTPUT'] + suff + '_fit_result.fits', overwrite=True)
    
    return rvmap


def samel(filename, plot=False, debug=False, free_ratio=False, multi_comp=1, multi_ext=False, config=None, cut=False, smooth=False, ftol=1e-7, xtol=1e-7, gtol=1e-7, factor=10, maxiter=200, sdss=False):
    """SAMEL (Spectrum Analysis: Moment of Emission Lines) computes moments

    Parameters
    ----------
    filename: string
        name of the configuration file
    plot: bool
        keyword to plot fits during the process (not implemented yet)
    debug: bool
        keyword to show debug information (not implemented yet)
    free_ratio: bool
        keyword to allow unconstrained line ratio
    multi_comp: int
        keyword to indicate the number of components to be adjusted
    multi_ext: bool
        keyword to indicate if input fits has multiple extensions
    config: dictionary
        optional dictionary that contains options usually stored in the configuration file

    """
    # XXX gérer message d'erreur si on trouve pas le fichier d'input, s'il n'y a aucune raie, si un paramètre a un problème (e.g. de formatage)...
    # XXX gérer cas avec une LSF donnée en input (non bruitée)
    
    t0 = time.time()
    
    #Reading of configuration file
    logger.info("Reading configuration file " + filename)
    try:
        conf = readconfig(filename, config)
    except:
        logger.info("Configuration file problem")
        return
    
    #Reading data
    try:
        logger.info("Reading data spectrum " + conf['FITSFILE'] + " and variance " + conf['SKYFILE'])
    except:
        logger.info("Reading data spectrum " + conf['FITSFILE'] + ". No variance data.")
    try:
        if sdss:
            print('SDSS', sdss)
            data, hdr, var, varhdr = readdata_sdss(conf)
        else:
            data, hdr, var, varhdr = readdata(conf, multi_ext=multi_ext)
    except:
        logger.info("Input spectrum problem")
        return
    
    try:
        testdata(data, var)
    except:
        logger.info("Variance size problem")
        #logger.info("Variance shape: %i; Data cube shape: %i"%(np.shape(var), data.shape))
        return
    
    #XXX when no variance, I should add an option to compute the variance from the data itself (as done in the IDL code)
    #Cutting the data
    logger.info("Cutting data spectrum")
    data, hdr = cutspectrum(data, hdr, conf)
    hdr = writedata(data, hdr, conf['OUTPUT'] + '_spectrum_cut.fits')
    if var is not None:
        logger.info("Cutting variance")
        var, varhdr = cutspectrum(var, varhdr, conf)
        varhdr = writedata(var, varhdr, conf['OUTPUT'] + '_var_cut.fits')
    if cut & (not(smooth)):
        return
    
    #Smoothing the data
    if (conf['WSMOOTH'] != 0) & (conf['WSMOOTH'] is not None):
        logger.info('Performing %3.1f pixels 1D spectral smoothing' % conf['WSMOOTH'])
        data, hdr = spectralsmooth(data, hdr, conf['WSMOOTH'])
        conf['OUTPUT'] = conf['OUTPUT'] + '_wsmooth'
        hdr = writedata(data, hdr, conf['OUTPUT'] + '_spectrum.fits')
        if var is not None:
            #var, varhdr = spectralsmooth(var, varhdr, conf['WSMOOTH'])
            ## variance is divided by the integral of the kernel to account for the variance lowering compared to the signal when smoothed
            #var /= ((conf['WSMOOTH'] / (2. * np.sqrt(2. * np.log(2.)))) * np.sqrt(2 * np.pi))
            var, varhdr = var_spectralsmooth(var, varhdr, conf['WSMOOTH'])
            varhdr = writedata(var, varhdr, conf['OUTPUT'] + '_var.fits')
    
    if smooth:
        return
        
    #Creating the maps
    rv = buildmaps(conf, data, hdr, var=var, plot=plot, debug=debug, free_ratio=free_ratio, multi_comp=multi_comp, ftol=ftol, xtol=xtol, gtol=gtol, factor=factor, maxiter=maxiter)

    t1 = time.time()
    #print(t1-t0)
    
    return


#def usage(option, opt, value, parser):
def usage():
    """SAMEL (Spectrum Analysis: Moments of Emission Lines) can be launched from command line
    or from a python console.
    
    #########################
    #Python console solution#
    #########################
    
    Syntaxe:
    --------
    import samel
    samel(filename, plot=False, debug=False, free_ratio=False, multi_ext=False, config=None)
    
    Parameters
    ----------
    filename: string
        name of the configuration file
    plot: bool
        keyword to plot fits during the process (not implemented yet)
    debug: bool
        keyword to show debug information (not implemented yet)
    free_ratio: bool
        keyword to allow unconstrained line ratio
    multi_ext: bool
        keyword to indicate if input fits has multiple extensions
    config: dictionary
        optional dictionary that contains options usually stored in the configuration file

    
    #######################
    #Command line solution#
    #######################
    
    Syntaxe:
    --------
    
    """
    print(usage.__doc__)


def main(argv):
    """
    """
    parser = argparse.ArgumentParser(description=usage())
    parser.add_argument('--file', '-f', action="store", dest="filename", default='', help="name of the configuration file")
    parser.add_argument('--plot', '-p', action="store_true", dest="plot", default=False, help="keyword to plot fits during the process (not implemented yet)")
    parser.add_argument('--debug', '-d', action="store_true", dest="debug", default=False, help="keyword to show debug information (not implemented yet)")
    parser.add_argument('--free_line_ratio', '-r', action="store_true", dest="free_ratio", default=False, help="keyword to allow unconstrained line ratio")
    parser.add_argument('--multi_comp', '-c', action="store", dest="multi_comp", default=1, type=int, help="Number of components")
    parser.add_argument('--multi_ext', '-m', action="store_true", dest="multi_ext", default=False, help="keyword to indicate if input fits has multiple extensions")
    parser.add_argument('--ftol', action="store", dest="ftol", default=1e-7, type=float, help="ftol argument in mpfit")
    parser.add_argument('--xtol', action="store", dest="xtol", default=1e-7, type=float, help="xtol argument in mpfit")
    parser.add_argument('--gtol', action="store", dest="gtol", default=1e-7, type=float, help="gtol argument in mpfit")
    parser.add_argument('--maxiter', action="store", dest="maxiter", default=200, type=int, help="maxiter argument in mpfit")
    parser.add_argument('--factor', action="store", dest="factor", default=10, type=float, help="factor argument in mpfit (between 0.1 and 100)")
    parser.add_argument('--FITSFILE', action="store", dest="FITSFILE", default=None, help="name of the input spectrum")
    parser.add_argument('--OUTPUT', action="store", dest="OUTPUT", default=None, help="generic output name")
    parser.add_argument('--SKYFILE', action="store", dest="SKYFILE", default=None, help="name of the variance spectrum (can be a sky spectrum)")
    parser.add_argument('--HALPHA', action="store_true", dest="HALPHA", default=None, help="keyword to fit Halpha@6562.801 line")
    parser.add_argument('--HBETA', action="store_true", dest="HBETA", default=None, help="keyword to fit Hbeta@4861.363 line")
    parser.add_argument('--HGAMMA', action="store_true", dest="HGAMMA", default=None, help="keyword to fit Hgamma@4340.47 line")
    parser.add_argument('--HDELTA', action="store_true", dest="HDELTA", default=None, help="keyword to fit Hdelta@4101.73 line")
    parser.add_argument('--HEPS', action="store_true", dest="HEPS", default=None, help="keyword to fit Hepsilon@3970.07 line")
    parser.add_argument('--NII6548', action="store_true", dest="NII6548", default=None, help="keyword to fit NII@6548.05 line")
    parser.add_argument('--NII6583', action="store_true", dest="NII6583", default=None, help="keyword to fit NII@6583.45 line")
    parser.add_argument('--SII6716', action="store_true", dest="SII6716", default=None, help="keyword to fit SII@6716.44 line")
    parser.add_argument('--SII6731', action="store_true", dest="SII6731", default=None, help="keyword to fit SII@6730.82 line")
    parser.add_argument('--OIII4363', action="store_true", dest="OIII4363", default=None, help="keyword to fit OIII@4363 line")
    parser.add_argument('--OIII4959', action="store_true", dest="OIII4959", default=None, help="keyword to fit OIII@4958.911 line")
    parser.add_argument('--OIII5007', action="store_true", dest="OIII5007", default=None, help="keyword to fit OIII@5006.843 line")
    parser.add_argument('--OII', action="store_true", dest="OII3729", default=None, help="keyword to fit OII doublet @3726.04,3728.80")  # ['OII3729','OII3726']
    parser.add_argument('--OI6300', action="store_true", dest="OI6300", default=None, help="keyword to fit OI@6300 line")
    parser.add_argument('--HEI4471', action="store_true", dest="HEI4471", default=None, help="keyword to fit HeI@4471 line")
    parser.add_argument('--HEI5876', action="store_true", dest="HEI5876", default=None, help="keyword to fit HeI@5876 line")
    parser.add_argument('--HEII4686', action="store_true", dest="HEII4686", default=None, help="keyword to fit HeII@4686 line")
    parser.add_argument('--NEIII3868', action="store_true", dest="NEIII3868", default=None, help="keyword to fit NeIII@3868 line")
    #parser.add_argument('--EXTRAL', action="append", dest="EXTRAL", default=None, type=float, help="keyword to add a new line (wavelength in angstrom) - multiple extra wavelengths can be added")
    parser.add_argument('--EXTRAL', action="append", nargs='*', default=None, help="keyword to add a new line. Can be provided, in this order: wavelength in Angstroms, reference line, theoretical, lower and upper ratios - multiple extra wavelengths can be added")
    parser.add_argument('--COMMW', action="store_true", dest="COMMW", default=None, help="keyword to fix common widths (in velocity) between several fitted lines")
    parser.add_argument('--REDSHIFT', action="store", dest="REDSHIFT", default=None, type=float, help="mean redshift of the object")
    parser.add_argument('--REDMIN', action="store", dest="REDMIN", default=None, type=float, help="minimum redshift allowed for fitting")
    parser.add_argument('--REDMAX', action="store", dest="REDMAX", default=None, type=float, help="maximum redshift allowed for fitting")
    parser.add_argument('--ZRMIN', action="store", dest="ZRMIN", default=None, type=float, help="minimum redshift for spectral range considered around lines")
    parser.add_argument('--ZRMAX', action="store", dest="ZRMAX", default=None, type=float, help="maximum redshift for spectral range considered around lines")
    parser.add_argument('--INITW', action="store", dest="INITW", default=None, type=float, help="initial line width in km/s")
    parser.add_argument('--WMIN', action="store", dest="WMIN", default=None, type=float, help="minimum width allowed for fitting in km/s")
    parser.add_argument('--WMAX', action="store", dest="WMAX", default=None, type=float, help="maximum width allowed for fitting in km/s")
    parser.add_argument('--DFIT', action="store", dest="DFIT", default=None, type=float, help="bin in km/s for searching the position of the lines")
    parser.add_argument('--DGCTNUM', action="store", dest="DGCTNUM", default=None, type=int, help="continuum degree")
    parser.add_argument('--LSF', action="store", dest="SPSF", default=None, type=float, help="spectral PSF width in angstroms (dispersion of the gaussian)")
    parser.add_argument('--WSMOOTH', action="store", dest="WSMOOTH", default=None, type=float, help="width for Gaussian spectral smoothing in pixel")  # options? gaussian uniquement?
    parser.add_argument('--ZMIN', action="store", dest="ZMIN", default=None, type=int, help="lower cut window in z direction of the spectrum in pixel")
    parser.add_argument('--ZMAX', action="store", dest="ZMAX", default=None, type=int, help="upper cut window in z direction of the spectrum in pixel")
    parser.add_argument('--cut', action="store_true", dest="cut", default=False, help="keword to stop after cutting the spectrum")
    parser.add_argument('--smooth', action="store_true", dest="smooth", default=False, help="keyword to stop after smoothing the spectrum")
    parser.add_argument('--sdss', action="store_true", dest="sdss", default=False, help="keyword to indicate that the file is SDSS type (fits table with columns, with loglam")

    #parser.add_argument('--MFIT', action="store", dest="MFIT", default=None)
    #parser.add_argument('--THRES', action="store", dest="THRES", default=None, type=float)
    #parser.add_argument('--MEDIAN', action="store", dest="MEDIAN", default=None, type=float)
    #parser.add_argument('--FITSPSF', action="store", dest="FITSPSF", default=None, type=float)
    
    try:
        options = parser.parse_args()
    except:
        sys.exit(2)
    
    if np.size(argv) < 1:
        sys.exit(2)
    
    opts = vars(options)   # this is a dictionary
    opts['OII3726'] = opts['OII3729']
    samel(options.filename, plot=options.plot, debug=options.debug, free_ratio=options.free_ratio, multi_comp=options.multi_comp, multi_ext=options.multi_ext, ftol=options.ftol, xtol=options.xtol, gtol=options.gtol, maxiter=options.maxiter, factor=options.factor, config=opts, cut=options.cut, smooth=options.smooth, sdss=options.sdss)
    return

if __name__ == "__main__":
    main(sys.argv[1:])
