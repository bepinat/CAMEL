#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on 12/2014
@author: Benoît Epinat
"""

import time
import os
import sys
import copy
import numpy as np
import astropy.io.fits as fits
from cap_mpfit import mpfit  # python 2.7 & 3!
#from mpfit import mpfit  # python 2.7...
#from matplotlib import pyplot as plt
from scipy import ndimage
import astropy.constants as ct

import argparse
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('CAMEL')


def dz_from_dv(dv, z):
    '''
    This function enables to compute the redshift interval corresponding to a given velocity interval
    
    Parameters
    ----------
    dv: float
        velocity interval in km/s
    z: float
        mean redshift of the source
    
    Returns
    -------
    dz: float
        corresponding redshift interval
    '''
    dz = dv / ct.c.to('km/s').value * (1 + z)
    return dz


def compute_rv(z, zcosmo=None):
    """This function computes the velocity from local and global redshifts

    Parameters
    ----------
    z: float (or numpy array of floats)
        local redshift (local velocity)
    zcosmo: float
        cosmological redshift (sytemic velocity)

    """

    if zcosmo is None:
        v = ((z + 1) ** 2 - 1) / ((z + 1) ** 2 + 1) * ct.c.to('km/s').value
    else:
        v = (z - zcosmo) / (1 + z) * ct.c.to('km/s').value
    return v


def compute_fwhm(dz, z=None, zcosmo=0):
    """This function computes the velocity variation

    Parameters
    ----------
    dz: float (or numpy array of floats)
        local redshift variation (local velocity dispersion)
    z: float (or numpy array of floats)
        local redshift (local velocity)
    zcosmo: float
        cosmological redshift (sytemic velocity)

    """

    if z is None:
        z = zcosmo
    dv = dz * (1 + zcosmo) / (1 + z) ** 2 * ct.c.to('km/s').value
    return dv


def readconfig(filename, config):
    """This function reads the configuration file and returns a configuration dictionary

    Parameters
    ----------
    filename: string
        name of the configuration file
    config: dictionary
        contains options with priority on the configuration file

    """

    conf = {}
    if filename is not None:
        try:
            data = open(filename)
        except:
            logger.info('Camel is not able to open ' + filename)
            data = open(filename)
        count_extral = 0
        for raw in data:
            keyvalcom = raw.split('= ')
            key = keyvalcom[0].rstrip()   # on vire les espaces de fin de keyword
            value = keyvalcom[1].split('/ ')[0].rstrip('\t').rstrip()  # on vire les tabultation et les espaces de fin de keyword
            value = value.replace("'", '')
            value = value.replace('"', '')
            if value == '':
                value = None
            elif value.upper() == 'NONE':
                value = None
            elif value.upper() == 'FALSE':
                value = False
            elif value.upper() == 'TRUE':
                value = True
            #elif (value.count('.') == 1) & (value.replace('.', '').isdigit()):
                #value = float(value)
            elif (value.count('.') == 1):
                try:
                    value = float(value)
                except:
                    value = value
            #elif value.isdigit():
                #value = int(value)
            elif (value.count('.') == 0):
                try:
                    value = int(value)
                except:
                    value = value
            if (key == 'EXTRAL') & (value is not None):
                if count_extral == 0:
                    conf[key] = list()
                conf[key].append([value])
                count_extral += 1
            else:
                conf[key] = value

    # add input options to configuration dictionary + check needed keywords
    if config is not None:
        for key in config:
            if config[key] is not None:
                conf[key] = config[key]
            elif not(key in conf.keys()):
                conf[key] = None
                # XXX or give an error message but in that case, must test that the keywords have correct values
            
    # XXX more keywords? add default values?
    needed_keys = ['FITSFILE', 'OUTPUT', 'SKYFILE', 'HALPHA', 'NII6548', 'NII6583', 'SII6716', 'SII6731', 'OIII4959', 'OIII5007', 'HBETA', 'OII3729', 'OII3726', 'EXTRAL', 'COMMW', 'REDSHIFT', 'REDMIN', 'REDMAX', 'ZRMIN', 'ZRMAX', 'INITW', 'WMIN', 'WMAX', 'DFIT', 'DGCTNUM', 'MFIT', 'SCLIP', 'XYCLIP', 'NCLIP', 'SPSF', 'WSMOOTH', 'SSMOOTH', 'BINNING', 'THRES', 'MEDIAN', 'FITSPSF', 'XMIN', 'YMIN', 'ZMIN', 'XMAX', 'YMAX', 'ZMAX']
    for key in needed_keys:
        if not(key in conf.keys()):
            conf[key] = None
    
    if 'OII' in conf.keys():
        conf['OII3729'] = conf['OII3726'] = conf.pop('OII')
    if conf['SPSF'] is None:
        conf['SPSF'] = 0.
    if conf['COMMW'] is None:
        conf['COMMW'] = False
        # XXX or give an error message but in that case, must test that the keywords have correct values
    if conf['ZRMIN'] is None:
        deltared = conf['REDMAX'] - conf['REDMIN']
        conf['ZRMIN'] = conf['REDMIN'] - deltared / 2.
    if conf['ZRMAX'] is None:
        deltared = conf['REDMAX'] - conf['REDMIN']
        conf['ZRMAX'] = conf['REDMAX'] + deltared / 2.
    
    return conf


def readcubes(conf, multi_ext=False):
    """This function reads the input cubes (cube and variance)

    Parameters
    ----------
    conf: configuration dictionary
    multi_ext: bool
        keyword to indicate if input fits has multiple extensions
    
    """
    
    if multi_ext:
        hdul = fits.open(conf['FITSFILE'])
        cube = hdul[1].data
        hdr = hdul[1].header
        if conf['SKYFILE'] == conf['FITSFILE']:
            var = hdul[2].data
            varhdr = hdul[2].header
        elif (not conf['SKYFILE']):
            var = None
            varhdr = ''
        else:
            hdul = fits.open(conf['SKYFILE'])
            var = hdul[0].data
            varhdr = hdul[0].header
    else:
        hdul = fits.open(conf['FITSFILE'])
        cube = hdul[0].data
        hdr = hdul[0].header
        if conf['SKYFILE'] == conf['FITSFILE']:
            var = np.zeros(cube.shape, dtype='>f4')
            if conf['XMIN'] is None:
                conf['XMIN'] = 0
            if conf['YMIN'] is None:
                conf['YMIN'] = 0
            if conf['XMAX'] is None:
                conf['XMAX'] = cube.shape[2] - 1
            if conf['YMAX'] is None:
                conf['YMAX'] = cube.shape[1] - 1
            for i in np.arange(cube.shape[0]):
                var[i, :, :] = np.var(cube[i, conf['YMIN']:conf['YMAX'], conf['XMIN']:conf['XMAX']])
            varhdr = hdul[0].header
            var = writedata(var, varhdr, conf['OUTPUT'] + '_variance.fits')
        elif (not conf['SKYFILE']):
            var = None
            varhdr = ''
        else:
            hdul = fits.open(conf['SKYFILE'])
            var = hdul[0].data
            varhdr = hdul[0].header
    return cube, hdr, var, varhdr


def testcubes(cube, var):
    """This function tests the sizes of the variance data

    Parameters
    ----------
    cube: ndarray
        input data cube
    var: ndarray
        input variance data
    
    """
    if var is None:
        return
    if var.ndim == 3:
        if var.shape != cube.shape:
            sys.exit(2)
    if var.ndim == 1:
        if var.shape[0] != cube.shape[0]:
            sys.exit(2)
    return


def writedata(data, hdr, filename):
    """This function writes the output data

    Parameters
    ----------
    data: ndarray
        data to be written
    hdr: fits header
        fits header to be written
    filename: string
        name of the output
    
    """
    hdu = fits.PrimaryHDU(data, hdr)
    hdulist = fits.HDUList(hdu)
    hdulist.writeto(filename, overwrite=True, output_verify='fix')
    return hdulist[0].header


def muse_whitelight_image(cube, hdr, filename):
    """This function creates and writes a white light image

    Parameters
    ----------
    data: ndarray
        cube to be collapsed
    hdr: fits header
        fits header of the cube
    filename: string
        name of the output
    
    """
    
    # Suppressing header keywords about spectral axis
    hdrw = copy.deepcopy(hdr)
    if 'CTYPE3' in hdr.keys():
        del hdrw['CTYPE3']
    if 'CRVAL3' in hdr.keys():
        del hdrw['CRVAL3']
    if 'CRPIX3' in hdr.keys():
        del hdrw['CRPIX3']
    if 'CUNIT3' in hdr.keys():
        del hdrw['CUNIT3']
    if 'CD3_3' in hdr.keys():
        del hdrw['CD3_3']
    if 'CD3_2' in hdr.keys():
        del hdrw['CD3_2']
    if 'CD3_1' in hdr.keys():
        del hdrw['CD3_1']
    if 'CD2_3' in hdr.keys():
        del hdrw['CD2_3']
    if 'CD1_3' in hdr.keys():
        del hdrw['CD1_3']
    if 'CDELT3' in hdr.keys():
        del hdrw['CDELT3']
    
    # Recovery of important information from header
    if 'BUNIT' in hdr.keys():
        bunit = hdr['BUNIT']
    else:
        bunit = ''
    wave, wn, lconvfac, sclp = waveindgen(hdr)
    
    # We put a meaningful quantity and unit (integration of signal)
    data = np.sum(cube, axis=0) * lconvfac * sclp
    hdrw['BUNIT'] = '{} {}'.format(bunit, 'Angstrom')
    
    wlih = writedata(data, hdrw, filename)
    return wlih


def cutcube(cube, hdr, conf):
    """This function cuts the cube according to the limits requested

    Parameters
    ----------
    cube: array
        input data cube or variance array
    hdr: fits header
        input data cube header
    conf: configuration dictionary

    """
    
    if conf['XMIN'] is None:
        conf['XMIN'] = 0
    if conf['YMIN'] is None:
        conf['YMIN'] = 0
    if conf['ZMIN'] is None:
        conf['ZMIN'] = 0
    if conf['XMAX'] is None:
        conf['XMAX'] = cube.shape[2] - 1
    if conf['YMAX'] is None:
        conf['YMAX'] = cube.shape[1] - 1
    if conf['ZMAX'] is None:
        conf['ZMAX'] = cube.shape[0] - 1
    
    if cube.ndim == 3:
        conf['ZMIN'], conf['YMIN'], conf['XMIN'] = np.max([[conf['ZMIN'], conf['YMIN'], conf['XMIN']], [0, 0, 0]], axis=0)
        conf['ZMAX'], conf['YMAX'], conf['XMAX'] = np.min([[conf['ZMAX'], conf['YMAX'], conf['XMAX']], np.array(cube.shape) - 1], axis=0)
        conf['ZMIN'], conf['YMIN'], conf['XMIN'] = np.min([[conf['ZMIN'], conf['YMIN'], conf['XMIN']], [conf['ZMAX'], conf['YMAX'], conf['XMAX']]], axis=0)
        conf['ZMAX'], conf['YMAX'], conf['XMAX'] = np.max([[conf['ZMAX'], conf['YMAX'], conf['XMAX']], [conf['ZMIN'], conf['YMIN'], conf['XMIN']]], axis=0)
    if cube.ndim == 1:
        conf['ZMIN'] = np.max([conf['ZMIN'], 0], axis=0)
        conf['ZMAX'] = np.min([conf['ZMAX'], np.array(cube.shape) - 1], axis=0)
        conf['ZMIN'] = np.min([conf['ZMIN'], conf['ZMAX']], axis=0)
        conf['ZMAX'] = np.max([conf['ZMAX'], conf['ZMIN']], axis=0)
    if cube.ndim == 3:
        cubecut = cube[int(conf['ZMIN']):int(conf['ZMAX']) + 1, int(conf['YMIN']):int(conf['YMAX']) + 1, int(conf['XMIN']):int(conf['XMAX']) + 1]
        hdr['CRPIX1'] -= conf['XMIN']
        hdr['CRPIX2'] -= conf['YMIN']
        hdr['CRPIX3'] -= conf['ZMIN']
    elif cube.ndim == 1:
        cubecut = cube[int(conf['ZMIN']):int(conf['ZMAX']) + 1]
        hdr['CRPIX1'] -= conf['ZMIN']
    return cubecut, hdr


def remove_nanvar(var):
    """This function removes nan in variance cube if any and replaces them by median variance (spectrally)
    
    Parameters
    ----------
    var: array
        input variance cube
    
    Returns
    -------
    corrected variance

    """
    
    cnan = np.isnan(var)  # True where there are NaN
    #nanmap = cnan.prod(axis=0)  # 0 if there is at least one value
    #indnan = np.where(nanmap == 1)
    #indnum = np.where(nanmap == 0)
    ncnan = np.logical_not(cnan)  # True where there are values
    nanmap = ncnan.prod(axis=0)  # 0 if there is at least one NaN
    indnan = np.where(nanmap == 0)  # indicates the pixels where there is at least one NaN
    indnum = np.where(nanmap == 1)  # indicates the pixels where there are no NaN
    qvar = var[:, indnum[0], indnum[1]].reshape(var.shape[0], np.size(indnum[0]))  # spectra with no NaN
    medianvar = np.median(qvar, axis=1)  # median variance spectrum
    if np.size(indnan[0]) == 0:
        logger.info('Variance cube contains NaN and has been corrected')
    for ind in range(np.size(indnan[0])):
        var[:, indnan[0][ind], indnan[1][ind]] = medianvar  # If there is one NaN, the whole variance spectrum is replaced by the median variance spectrum
    return var


def clipcube(cube, hdr, xy=3, sclip=3, npass=10):
    """This function performs a sigma clipping on the cube

    Parameters
    ----------
    cube: array
        input datacube
    hdr: fits header
        input data cube header
    xy: integer
        size of the box in pixels in which the clipping is done
    sclip: float
        value of the clipping (n sigma)
    npass: integer
        number of pass to perform the clipping
    
    """

    cube1 = np.zeros(cube.shape, dtype='>f4')   # initialization of output cube
    for z in range(cube.shape[0]):  # loop on the z range
        im = cube[z, :]
        count = 0
        ok = False
        #while count < npass | ok:
        while (count < npass) & (not(ok)):
            count += 1
            im1 = ndimage.median_filter(im, xy)
            diff = im - im1
            bad = np.abs(diff) > (sclip * np.std(diff))
            im[bad] = im1[bad]
            #if np.size(bad) == 0:
            if np.sum(bad) == 0:
                ok = True
        cube1[z, :] = im
    hdr.append(('XYCLIP', xy, 'Size of the clipping box'))
    hdr.append(('SCLIP', sclip, 'n-sigma clipping'))
    hdr.append(('NCLIP', npass, 'Number of pass of the clipping process'))
    return cube1, hdr


def rebin(data, rebin_factors):
    '''
    Function that rebins data, any dimension
    
    Parameters:
    -----------
    data: ndarray
        data to be rebinned
    rebin_factors: scalar (int) or array-like of ints
        Rebin factor used to bin the data. In case of a scalar, the same factor is applied on any axis. Otherwise, the length of the array has to match the number of dimensions of the image
    
    Returns
    -------
    Rebinned data
    '''
    if len(rebin_factors) == 1:
        rebin_factors *= np.ones(data.ndim, dtype=np.int32)
        
    if len(rebin_factors) != data.ndim:
        print('Incorrect length of rebin factors')
        return None
    
    data2 = np.zeros(((data.shape[0] // rebin_factors[0]) * rebin_factors[0], (data.shape[1] // rebin_factors[1]) * rebin_factors[1]))
    data2[:,:] = data[:data2.shape[0],:data2.shape[1]]
    
    reshape_array = np.zeros(data2.ndim * 2, dtype=np.int32)
    sum_axis = np.arange(data2.ndim, dtype=np.int32) * 2 + 1
    norm = 1
    for i in range(data.ndim):
        reshape_array[i * 2] = data2.shape[i] / rebin_factors[i]
        reshape_array[i * 2 + 1] = rebin_factors[i]
        #norm /= rebin_factors[i]
    data_bin = data2.reshape(reshape_array).sum(axis=tuple(sum_axis)) * norm
    
    # TBD faut-il normaliser au bien sommer? Pour l'instant, je somme, comme ça ça fonctionne avec la variance
    
    return data_bin, norm


def spatialbin(cube, hdr, binning):
    """This function performs a spatial binning of the cube

    Parameters
    ----------
    cube: array
        input datacube
    hdr: fits header
        input data cube header
    binning: int
        size of the binning box (square)
    """
    
    cube1 = np.zeros((cube.shape[0], cube.shape[1] // binning, cube.shape[2] // binning), dtype='>f4')   # initialization of output cube
    
    for z in range(cube.shape[0]):  # loop on the z range
        cube1[z, :], norm = rebin(cube[z, :], [binning])
    
    hdr['CDELT1'] *= binning
    hdr['CDELT2'] *= binning
    
    hdr['CRPIX1'] = (hdr['CRPIX1'] - 0.5) / binning + 0.5
    hdr['CRPIX2'] = (hdr['CRPIX2'] - 0.5) / binning + 0.5
    
    return cube1, hdr


def var_spatialbin(cube, hdr, binning):
    """This function performs a spatial binning of the variance

    Parameters
    ----------
    cube: array
        input datacube
    hdr: fits header
        input data cube header
    binning: int
        size of the binning box (square)
    """
    
    cube1 = np.zeros((cube.shape[0], cube.shape[1] // binning, cube.shape[2] // binning), dtype='>f4')   # initialization of output cube
    
    for z in range(cube.shape[0]):  # loop on the z range
        cube1[z, :], norm = rebin(cube[z, :], [binning])
    
    cube1[z, :] *= norm  # for variance
    
    hdr['CDELT1'] *= binning
    hdr['CDELT2'] *= binning
    
    return cube1, hdr


def spatialsmooth(cube, hdr, fwhm):
    """This function performs a Gaussian spatial smoothing on the cube

    Parameters
    ----------
    cube: array
        input datacube
    hdr: fits header
        input data cube header
    fwhm: float
        full width half maximum of the 2D Gaussian kernel
    """
    
    cube1 = np.zeros(cube.shape, dtype='>f4')   # initialization of output cube
    sigma = fwhm / (2. * np.sqrt(2. * np.log(2.)))
    for z in range(cube.shape[0]):  # loop on the z range
        cube1[z, :] = ndimage.gaussian_filter(cube[z, :], sigma)
    hdr.append(('SSMOOTH', fwhm, 'FWHM in pixels of 2D Gaussian spatial smoothing'))
    return cube1, hdr


def var_spatialsmooth(cube, hdr, fwhm):
    """This function performs a Gaussian spatial smoothing on the cube

    Parameters
    ----------
    cube: array
        input datacube
    hdr: fits header
        input data cube header
    fwhm: float
        full width half maximum of the 2D Gaussian kernel
    """
    
    cube1 = np.zeros(cube.shape, dtype='>f4')   # initialization of output cube
    sigma = fwhm / (2. * np.sqrt(2. * np.log(2.)))
    # For variance, we have to convolve with the square of the kernel.
    # In 2D, it is equivalent to convolve by a kernel of width sigma2 = sigma / np.sqrt(2), hence FWHM2 = FWHM/np.sqrt(2)
    # and to renormalise the amplitude by multiplying by a factor fac = 1 / (2 * sigma) / np.sqrt(np.pi)
    sigma2 = sigma / np.sqrt(2)
    fac = 1 / (4 * sigma**2 * np.pi)

    for z in range(cube.shape[0]):  # loop on the z range
        cube1[z, :] = ndimage.gaussian_filter(cube[z, :], sigma2)
    cube1 *= fac
    hdr.append(('SSMOOTH', fwhm, 'FWHM in pixels of 2D Gaussian spatial smoothing'))
    return cube1, hdr


def spectralsmooth(cube, hdr, fwhm):
    """This function performs a Gaussian spectral smoothing on the cube

    Parameters
    ----------
    cube: array
        input datacube
    hdr: fits header
        input data cube header
    fwhm: float
        full width half maximum of the 1D Gaussian kernel
    """
    
    cube1 = np.zeros(cube.shape, dtype='>f4')   # initialization of output cube
    sigma = fwhm / (2. * np.sqrt(2. * np.log(2.)))
    for x in range(cube.shape[2]):  # loop on the x range
        for y in range(cube.shape[1]):  # loop on the y range
            cube1[:, y, x] = ndimage.gaussian_filter(cube[:, y, x], sigma)
    hdr.append(('WSMOOTH', fwhm, 'FWHM in pixels of Gaussian spectral smoothing'))
    # XXX add Hanning, Gauss, ...
    return cube1, hdr


def var_spectralsmooth(cube, hdr, fwhm):
    """This function performs a Gaussian spectral smoothing on the cube

    Parameters
    ----------
    cube: array
        input datacube
    hdr: fits header
        input data cube header
    fwhm: float
        full width half maximum of the 1D Gaussian kernel
    """
    
    cube1 = np.zeros(cube.shape, dtype='>f4')   # initialization of output cube
    sigma = fwhm / (2. * np.sqrt(2. * np.log(2.)))
    # For variance, we have to convolve with the square of the kernel.
    # In 1D, it is equivalent to convolve by a kernel of width sigma2 = sigma / np.sqrt(2), hence FWHM2 = FWHM/np.sqrt
    # and to renormalise the amplitude by multiplying by a factor fac = 1 / (2 * sigma) / np.sqrt(np.pi)
    sigma2 = sigma / np.sqrt(2)
    fac = 1 / (2 * sigma * np.sqrt(np.pi))

    for x in range(cube.shape[2]):  # loop on the x range
        for y in range(cube.shape[1]):  # loop on the y range
            cube1[:, y, x] = ndimage.gaussian_filter(cube[:, y, x], sigma2)
    cube1 *= fac
    hdr.append(('WSMOOTH', fwhm, 'FWHM in pixels of Gaussian spectral smoothing'))
    # XXX add Hanning, Gauss, ...
    return cube1, hdr


class line:
    """This class is the basic for defining a line. A line is defined by its name, its wavelength and the reference line to which it is attached if the ratio has to be constrained.
    
    """
    def __init__(self, name, wave, ref=None, fit=False, low=0, up=None, th=None, index=None):
        self.index = index
        self.name = name
        self.wave = wave
        self.wavenumber = 1e8 / wave
        self.fit = fit
        if ref is None:
            ref = name
        self.ref = ref
        if ref == name:
            self.low = 1
            self.up = 1
            self.th = 1
        else:
            self.low = low
            self.up = up
            self.th = th


class lines:
    """This class enables to deal with lines.  A dictionary stored in lines will contain informations on each lines.
    
    """
    
    def append(self, line):
        self.lines[line.name] = line
        self.lines[line.name].index = self.index
        self.index += 1
        
    def __init__(self):
        self.index = 0
        self.lines = {}
        self.append(line('HALPHA', 6562.801, ref='HBETA', low=2.75, th=2.85))
        self.append(line('HBETA', 4861.363))
        self.append(line('HGAMMA', 4340.47, ref='HBETA', low=0.44, up=0.5, th=0.468))
        self.append(line('HDELTA', 4101.73, ref='HBETA', low=0.23, up=0.29, th=0.259))
        self.append(line('HEPS', 3970.07, ref='HBETA', low=0.13, up=0.19, th=0.159))
        self.append(line('NII6583', 6583.45, ref='NII6548', low=2.7, up=3.3, th=3.))
        #self.append(line('NII6583', 6583., ref='NII6548', low=2.7, up=3.3, th=3.))
        self.append(line('NII6548', 6548.05))
        self.append(line('SII6731', 6730.82))
        self.append(line('SII6716', 6716.44, ref='SII6731', low=0.45, up=1.45, th=1.))
        self.append(line('OIII5007', 5006.843, ref='OIII4959', low=2.7, up=3.3, th=3.))
        #self.append(line('OIII5007', 5006.843, ref='OIII4959', low=2.9, up=3.1, th=3.))
        self.append(line('OIII4959', 4958.911))
        self.append(line('OIII4363', 4363.21, ref='OIII4959'))  # XXX low=, up=, th=
        self.append(line('OII3729', 3728.80, ref='OII3726', low=0.35, up=1.5, th=1.))
        #self.append(line('OII3729', 3728.80, ref='OII3726', low=0.7, up=1.5, th=1.))
        #self.append(line('OII3729', 3728.80, ref='OII3726', low=0.95, up=1.05, th=1.))
        #self.append(line('OII3729', 3728.80, ref='OII3726', low=1.3, up=1.5, th=1.4))
        self.append(line('OII3726', 3726.04))
        self.append(line('HEI4471', 4471.))
        self.append(line('HEI5876', 5876., ref='HEI4471', low=2.5, th=2.5))
        self.append(line('HEII4686', 4686.))
        self.append(line('OI6300', 6300.3))
        self.append(line('NEIII3868', 3868.))
        
        #self['Ha'] =
        #self.names = ['Ha', 'Hb', 'Hga', 'Hd', 'Heps', 'NII6583', 'NII6548', 'SII6731', 'SII6716', 'OIII5007', 'OIII4959', 'OIII4363', 'OII3729', 'OII3726', 'HeI4471', 'HeI5876', 'HeII4686', 'OI6300', 'NeIII3868']
        #self.waves = [6562.8, 4861., 4340., 4101., 3968., 6583., 6548., 6731., 6717., 5007., 4959., 4363., 3729., 3726., 4471., 5876., 4686., 6300., 3868.]
        #self.ref = ['Hb', 'Hb', 'Hb', 'Hb', 'Hb', 'NII6548', 'NII6548', 'SII6731', 'SII6731', 'OIII4959', 'OIII4959', 'OIII4959', 'OII3726', 'OII3726', 'HeI4471', 'HeI4471', 'HeII4686', 'OI6300', 'NeIII3868']
        
        #lines={'Ha':6562.8,'Hb':4861.,'Hga':4340.,'Hd':4101.,'Heps':3968.,'NII6583':6583.,'NII6548':6548.,'SII6731':6731.,'SII6716':6717.,'OIII5007':5007.,'OIII4959':4959.,'OIII4363':4363.,'OII3729':3729.,'OII3726':3726.,'HeI4471':4471.,'HeI5876':5876.,'HeII4686':4686.,'OI6300':6300.,'NeIII3868':3868.}
        ##Initialisation of groups of lines
        #sgroup={'Ha':'Hb','Hb':'Hb','Hga':'Hb','Hd':'Hb','Heps':'Hb','NII6583':'NII6548','NII6548':'NII6548','SII6731':'SII6731','SII6716':'SII6731','OIII5007':'OIII4959','OIII4959':'OIII4959','OIII4363':'OIII4959','OII3729':'OII3726','OII3726':'OII3726','HeI4471':'HeI4471','HeI5876':'HeI4471','HeII4686':'HeII4686','OI6300':'OI6300','NeIII3868':'NeIII3868'}


def waveindgen(hdr):
    '''
    This function returns the wavelength index in angstroms from the header information and the conversion factor from header data to angstroms.
    '''
    
    cunit = hdr['CUNIT3']
    if cunit.strip().lower() in {'microns', 'micron', 'micrometers', 'mum', 'um'}:
        lconvfac = 1e4
        wavelength = True
    if cunit.strip().lower() in {'angstroms', 'angstrom'}:
        lconvfac = 1
        wavelength = True
    if cunit.strip().lower() in {'nanometers', 'nm'}:
        lconvfac = 1e1
        wavelength = True
    if cunit.strip().lower() in {'centimeters', 'cm'}:
        lconvfac = 1e8
        wavelength = True
    if cunit.strip().lower() in {'millimeters', 'mm'}:
        lconvfac = 1e7
        wavelength = True
    
    if cunit.strip().lower() in {'cm-1'}:
        lconvfac = 1
        wavelength = False
    if cunit.strip().lower() in {'hz'}:
        lconvfac = 1 / ct.c.to('cm/s')
        wavelength = False
    
    if 'CD3_3' in hdr.keys():
        sclp = hdr['CD3_3']
    elif 'CDELT3' in hdr.keys():
        sclp = hdr['CDELT3']
    
    if wavelength:
        wave = lconvfac * (sclp * (np.arange(hdr['NAXIS3']) - hdr['CRPIX3'] + 1) + hdr['CRVAL3'])  # Angstroms
        wn = 1e8 / wave  # cm-1
    else:
        wn = lconvfac * (sclp * (np.arange(hdr['NAXIS3']) - hdr['CRPIX3'] + 1) + hdr['CRVAL3'])  # cm-1
        wave = 1e8 / wn  # Angstroms
    
    return wave, wn, lconvfac, sclp


def elspectrum(p, wave=None, lines=None, psf=None, degc=None, zind=None, sigind=None, intind=None, ratioind=None, contind=None):
    wavel = wave.reshape(wave.size, 1)
    deg = np.arange(degc + 1)
    coefs = p[contind]
    continuum = (coefs * (wavel - wave[0]) ** deg).sum(axis=1)
    wavel1 = (lines * (p[zind] + 1))
    dlines = (p[sigind] * lines)
    coefs = (p[intind] * p[ratioind])
    slines = (coefs * np.exp(-0.5 * (wavel - wavel1) ** 2 / (dlines ** 2 + psf ** 2))).sum(axis=1)
    
    #wavel = wave.reshape(wave.size, 1)
    #deg = np.arange(degc + 1)
    #coefs = p[lines.size * 4:lines.size * 4 + degc + 1]
    #continuum = (coefs * (wavel - wave[0]) ** deg).sum(axis=1)
    #wavel1 = (lines * (p[0:lines.size] + 1))
    #dlines = (p[lines.size:lines.size * 2] * lines)
    #coefs = (p[lines.size * 2:lines.size * 3] * p[lines.size * 3:lines.size * 4])
    #slines = (coefs * np.exp( -0.5 * (wavel - wavel1) ** 2 / (dlines ** 2 + psf ** 2))).sum(axis=1)
    
    #wavel = wave
    #deg = np.arange(degc + 1).reshape(degc + 1, 1)
    #coefs = p[lines.size * 4:lines.size * 4 + degc + 1].reshape(degc + 1, 1)
    #continuum = (coefs * (wavel - wave[0]) ** deg).sum(axis=0)
    #wavel1 = (lines * (p[0:lines.size] + 1)).reshape(lines.size, 1)
    #dlines = (p[lines.size:lines.size * 2] * lines).reshape(lines.size, 1)
    #coefs = (p[lines.size * 2:lines.size * 3] * p[lines.size * 3:lines.size * 4]).reshape(lines.size, 1)
    #slines = (coefs * np.exp( -0.5 * (wavel - wavel1) ** 2 / (dlines ** 2 + psf ** 2))).sum(axis=0)

    #wavel = wave.reshape(1, wave.size)
    #deg = np.arange(degc + 1).reshape(degc + 1, 1)
    #coefs = p[lines.size * 4:lines.size * 4 + degc + 1].reshape(degc + 1, 1)
    #continuum = (coefs * (wavel - wave[0]) ** deg).sum(axis=0)
    #wavel1 = (lines * (p[0:lines.size] + 1)).reshape(lines.size, 1)
    #dlines = (p[lines.size:lines.size * 2] * lines).reshape(lines.size, 1)
    #coefs = (p[lines.size * 2:lines.size * 3] * p[lines.size * 3:lines.size * 4]).reshape(lines.size, 1)
    #slines = (coefs * np.exp( -0.5 * (wavel - wavel1) ** 2 / (dlines ** 2 + psf ** 2))).sum(axis=0)
    return (continuum + slines)


def myelspectrum(p, fjac=None, wave=None, spectrum=None, err=None, lines=None, psf=None, degc=None, zind=None, sigind=None, intind=None, ratioind=None, contind=None):
    #t1=time.time()
    model = elspectrum(p, wave=wave, lines=lines, psf=psf, degc=degc, zind=zind, sigind=sigind, intind=intind, ratioind=ratioind, contind=contind)
    status = 0
    #t2=time.time()
    #print(t2-t1)
    return [status, (spectrum - model) / err]


def build_parinfo_lines(els, linename, conf, free_ratio, comind, multi_comp=1):
    """
    els: emission lines (not organised)
    linename: name of lines organised
    conf: configuration parameters
    free_ratio:
    comind: Index of the reference line for redshift and eventually dispersion
    """
    zcosmo = conf['REDSHIFT']
    zmax = conf['REDMAX']
    zmin = conf['REDMIN']
    
    parbase = {'value': 0., 'fixed': 1, 'limited': [0, 0], 'limits': [0., 0.], 'tied': '', 'mpmaxstep': 0, 'mpminstep': 0, 'parname': ''}
    parinfo = []
    
    #Creation of parameter constraints for all lines
    for i in range(els.index * 4):
        parinfo.append(copy.deepcopy(parbase))
        
    #We set the redshift as free parameter for the reference line
    parinfo[comind]['fixed'] = 0
    
    #When common dispersion, we set the dispersion as free parameter for the reference line
    if conf['COMMW']:
        parinfo[els.index + comind]['fixed'] = 0

    for i in range(els.index):
        name = linename[i]
        j = els.lines[els.lines[name].ref].index
        
        #Redshift
        parinfo[i]['limited'] = [1, 1]
        parinfo[i]['limits'] = [zmin, zmax]
        #Redshift is common for all lines: tied to the first free line
        if (i != comind) & (els.lines[name].fit is True):
            parinfo[i]['tied'] = 'p[%i]' % (comind + multi_comp * len(parinfo))
        
        #Dispersion
        parinfo[els.index + i]['limited'] = [1, 1]
        parinfo[els.index + i]['limits'] = [(conf['WMIN'] / ct.c.to('km/s').value + zcosmo) / (1 - conf['WMIN'] / ct.c.to('km/s').value) - zcosmo, (conf['WMAX'] / ct.c.to('km/s').value + zcosmo) / (1 - conf['WMAX'] / ct.c.to('km/s').value) - zcosmo]  # in redshift unit
        parinfo[els.index + i]['value'] = (conf['INITW'] / ct.c.to('km/s').value + zcosmo) / (1 - conf['INITW'] / ct.c.to('km/s').value) - zcosmo
        
        #When dispersion is common for all lines, widths are tied to the first free line (the first free width is free)
        if (conf['COMMW']) & (i != comind) & (els.lines[name].fit is True):
            parinfo[els.index + i]['tied'] = 'p[%i]' % (els.index + comind + multi_comp * len(parinfo))
        
        #When dispersion is independant for all lines (common by groups), widths are tied to the reference line and the ref width is free
        if (not conf['COMMW']) & (i != j):
            parinfo[els.index + i]['tied'] = 'p[%i]' % (els.index + j + multi_comp * len(parinfo))
            if els.lines[name].fit:
                parinfo[els.index + j]['fixed'] = 0  # the width of the reference line is free
        if (not conf['COMMW']) & (i == j) & (els.lines[name].fit is True):
            parinfo[els.index + i]['fixed'] = 0  # the width of the reference line is free
            
        #XXX When dispersion is completely independant, widths are not tied and are free
        #if (not conf['COMMW']):
            #if els.lines[name].fit: parinfo[els.index + i]['fixed'] = 0 # the width of the reference line is free
        
        #Intensity
        parinfo[2 * els.index + i]['limited'][0] = 1
        parinfo[2 * els.index + i]['limits'][0] = 0.
        
        #When line ratio is free: all ratios are fixed (= 1) and lines intensities are not tied and free parameters
        if free_ratio:
            parinfo[3 * els.index + i]['value'] = 1
            if els.lines[name].fit is True:
                parinfo[2 * els.index + i]['fixed'] = 0
        
        #When line ratio is constrained: line intensities are tied and ratios are free, except for the reference line (the ref intensity is free, the ref ratio is fixed = 1)
        if (not free_ratio) & (i != j) & (els.lines[name].fit is True):
            parinfo[2 * els.index + i]['tied'] = 'p[%i]' % (2 * els.index + j + multi_comp * len(parinfo))  # line intensity is tied to reference
            #Ratio limits
            parinfo[3 * els.index + i]['limited'][0] = 1
            if els.lines[name].low is not None:
                parinfo[3 * els.index + i]['limits'][0] = els.lines[name].low
            if els.lines[name].up is not None:
                parinfo[3 * els.index + i]['limited'][1] = 1
                parinfo[3 * els.index + i]['limits'][1] = els.lines[name].up
            if els.lines[name].th is not None:
                parinfo[3 * els.index + i]['value'] = els.lines[name].th
                #params[3 * els.index + i] = els.lines[name].th
            else:
                parinfo[3 * els.index + i]['value'] = 1
                #params[3 * els.index + i] = 1
            
            #Free parameters
            if els.lines[name].fit is True:
                parinfo[2 * els.index + j]['fixed'] = 0  # the intensity of the reference line is free
                parinfo[3 * els.index + i]['fixed'] = 0  # line ratio is free
                #XXX When reference line is not fitted, we have to find a solution if several lines of a group are fitted
            
        if (not free_ratio) & (i == j) & (els.lines[name].fit is True):
            #When line is reference line, the ratio is fixed equal to 1
            parinfo[3 * els.index + i]['limited'] = [0, 0]
            parinfo[3 * els.index + i]['value'] = 1
            #params[3 * els.index + i] = 1
            parinfo[2 * els.index + i]['fixed'] = 0
                
        #XXX When line ratio is fixed: all ratios are fixed equal to theoretical values and intensities are tied (the ref intensity is free)
        #if (not free_ratio) & (i!=j):
            #parinfo[2 * els.index + i]['tied'] = 'p[%i]'%(2 * els.index + j + multi_comp * len(parinfo))
            #parinfo[3 * els.index + i]['limited'][0] = 1
            #if els.lines[name].low is not None:
                #parinfo[3 * els.index + i]['limits'][0] = els.lines[name].low
            #if els.lines[name].up is not None:
                #parinfo[3 * els.index + i]['limited'][1] = 1
                #parinfo[3 * els.index + i]['limits'][1] = els.lines[name].up
            #if els.lines[name].th is not None:
                #parinfo[3 * els.index + i]['value'] = params[3 * els.index + i] = els.lines[name].th
            #else:
                #params[3 * els.index + i] = 1
            #if els.lines[name].fit is True:
                #parinfo[2 * els.index + j]['fixed'] = 0 # the intensity of the reference line is free
            
        #if (not free_ratio) & (i == j) & (els.lines[name].fit is True):
            ##When line is reference line, the ratio is fixed equal to 1
            #parinfo[3 * els.index + i]['limited'] = [0, 0]
            #parinfo[3 * els.index + i]['value'] = params[3 * els.index + i] = 1
            #parinfo[2 * els.index + i]['fixed'] = 0
    return parinfo


def buildmaps(conf, cube, hdr, var=None, plot=False, debug=False, free_ratio=False, multi_comp=1, factor=10, ftol=1e-7, gtol=1e-7, xtol=1e-7, maxiter=200):
    """
    """

    #Initialisation of lines
    els = lines()
    for lline in els.lines:
        #check the line name is set in the configuration file
        if lline in conf.keys():
            els.lines[lline].fit = conf[lline]

    if conf['EXTRAL'] is not None:
        for i in np.arange(np.shape(conf['EXTRAL'])[0]):
            if np.shape(conf['EXTRAL'][i])[0] == 1:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True))
            if np.shape(conf['EXTRAL'][i])[0] == 2:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1]))
            if np.shape(conf['EXTRAL'][i])[0] == 3:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1], th=float(conf['EXTRAL'][i][2])))
            if np.shape(conf['EXTRAL'][i])[0] == 4:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1], th=float(conf['EXTRAL'][i][2]), low=float(conf['EXTRAL'][i][3])))
            if np.shape(conf['EXTRAL'][i])[0] == 5:
                els.append(line('EXTRAL%i' % (i+1), float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1], th=float(conf['EXTRAL'][i][2]), low=float(conf['EXTRAL'][i][3]), up=float(conf['EXTRAL'][i][4])))
            #if np.shape(conf['EXTRAL'][i])[0] == 6:
                #els.append(line(conf['EXTRAL'][i][5], float(conf['EXTRAL'][i][0]), fit=True, ref=conf['EXTRAL'][i][1], th=float(conf['EXTRAL'][i][2]), low=float(conf['EXTRAL'][i][3]), up=float(conf['EXTRAL'][i][4])))
    
    #Construction of wavelength index
    wave0, wn0, lconvfac, sclp = waveindgen(hdr)  # wavelength array in Angstrom and conversion factor

    #Condition on the wavelength to be within the redshift cuts
    #XXX try zmin > zmax?
    zmax = conf['ZRMAX']
    zmin = conf['ZRMIN']
    
    argsorted = np.argsort(np.array([els.lines[i].index for i in els.lines]))
    lineindex = np.array([els.lines[i].index for i in els.lines])[argsorted]
    linefit = np.array([els.lines[i].fit for i in els.lines])[argsorted]
    linewave = np.array([els.lines[i].wave for i in els.lines])[argsorted]
    linename = np.array([els.lines[i].name for i in els.lines])[argsorted]
    indok = np.zeros(0, dtype=int)
    firstind = np.zeros(linefit.size, dtype=int)
    lastind = np.zeros(linefit.size, dtype=int)
    firstind0 = np.zeros(linefit.size, dtype=int)
    lastind0 = np.zeros(linefit.size, dtype=int)
    for i in range(linefit.size):
        if (not linefit[i]):
            continue
        linf = np.max([(zmin + 1) * linewave[i], np.min(wave0)])
        lsup = np.min([(zmax + 1) * linewave[i], np.max(wave0)])
        condo = np.where((wave0 >= linf) & (wave0 <= lsup))
        if np.size(condo) != 0:
            firstind0[i] = condo[0][0]
            lastind0[i] = condo[0][np.size(condo) - 1]
            indok = np.append(indok, condo)
    indok = np.unique(indok)
    wave = wave0[indok]
    
    for i in range(linefit.size):
        if (not linefit[i]):
            continue
        linf = np.max([(zmin + 1) * linewave[i], np.min(wave0)])
        lsup = np.min([(zmax + 1) * linewave[i], np.max(wave0)])
        condo = np.where((wave >= linf) & (wave <= lsup))
        if np.size(condo) != 0:
            firstind[i] = condo[0][0]
            lastind[i] = condo[0][np.size(condo) - 1]
    
    #firstind = np.zeros(np.size(cond))
    #lastind = np.zeros(np.size(cond))
    #firstind0 = np.zeros(np.size(cond))
    #lastind0 = np.zeros(np.size(cond))
    #for i in range(np.size(cond)):
        #linf = np.max([(zmin - zrange / 2. + 1) * linewave[cond[0][i]] , np.min(wave0)])
        #lsup = np.min([(zmax + zrange / 2. + 1) * linewave[cond[0][i]] , np.max(wave0)])
        #condo = np.where((wave0 >= linf) & (wave0 <= lsup))
        #if np.size(condo) != 0:
            #firstind0[i] = condo[0][0]
            #lastind0[i] = condo[0][np.size(condo) - 1]
            #indok = np.append(indok, condo)
    #indok = np.unique(indok)
    #wave = wave0[indok]
    
    #for i in range(np.size(cond)):
        #linf = np.max([(zmin - zrange / 2. + 1) * linewave[cond[0][i]] , np.min(wave0)])
        #lsup = np.min([(zmax + zrange / 2. + 1) * linewave[cond[0][i]] , np.max(wave0)])
        #condo = np.where((wave >= linf) & (wave <= lsup))
        #if np.size(condo) != 0:
            #firstind[i] = condo[0][0]
            #lastind[i] = condo[0][np.size(condo) - 1]

    #Weights
    #if var is None: var = np.ones([indok.size, cube.shape[1], cube.shape[2]])
    if var is None:
        var = np.ones(cube.shape, dtype='>f4')
    if var.ndim == 1:
        var = np.tile(var.reshape(var.size, 1, 1), (1, cube.shape[1], cube.shape[2]))
    #if var.ndim == 3: weight = ((1. / var[indok, :, :]) / np.sum(1. / var[indok, :, :], axis=0)) * indok.size
    # weight inverse variance
    #if var.ndim == 3: weight = ((1. / var) / np.sum(1. / var, axis=0)) * var.shape[0]
    # weight inverse standard deviation
    #var += 0
    if var.ndim == 3:
        weight = ((1. / np.sqrt(var)) / np.sum(1. / np.sqrt(var), axis=0)) * var.shape[0]
    
    #XXX try zmin > zmax?
    zcosmo = conf['REDSHIFT']
    zmax = conf['REDMAX']
    zmin = conf['REDMIN']
    
    #Redshift step
    zstep = dz_from_dv(conf['DFIT'], zcosmo)
    #zstep = (conf['DFIT'] / ct.c.to('km/s').value + zcosmo) / (1 - conf['DFIT'] / ct.c.to('km/s').value) - zcosmo
    
    #if zstep >= (zmax - zmin) / 2: zstep = (zmax - zmin) / 2.
    zmean = (zmax + zmin) / 2.
    
    ninter = int(np.floor((zmax - zmin) / zstep))  # number of intervals
    dzz = zstep * ninter
    zzmin = zmin + ((zmax - zmin) - dzz) / 2
    zzmax = zmax - ((zmax - zmin) - dzz) / 2
    logger.debug('REDMIN {}; REDMAX {}; DFIT {}; ZMIN {}; ZMAX {}; ZSTEP {}; NINTER {}: NSTEPS {}'.format(zmin, zmax, conf['DFIT'], zzmin, zzmax, zstep, ninter, ninter + 1))

    #Parameters constraints and information: each line has z, sigma, intensity and ratio constraint

    #Index of the reference line for redshift and eventually dispersion
    comind = np.min(lineindex[np.where(linefit)])
    
    #parinfo = build_parinfo_lines(els, linename, conf, free_ratio, comind)
    parinfo = []
    zind = []
    sigind = []
    intind = []
    ratioind = []
    #Lines parameters
    linewaves = np.array(())
    components = np.zeros(0, dtype='i')
    for comp in range(multi_comp):
        zind.extend(range(len(parinfo) + 0 * len(linewave), len(parinfo) + 1 * len(linewave)))
        sigind.extend(range(len(parinfo) + 1 * len(linewave), len(parinfo) + 2 * len(linewave)))
        intind.extend(range(len(parinfo) + 2 * len(linewave), len(parinfo) + 3 * len(linewave)))
        ratioind.extend(range(len(parinfo) + 3 * len(linewave), len(parinfo) + 4 * len(linewave)))
        parinfom = build_parinfo_lines(els, linename, conf, free_ratio, comind, multi_comp=comp)
        parinfo.extend(parinfom)
        linewaves = np.append(linewaves, linewave)
        components = np.append(components, np.ones(len(parinfom), dtype='i') * (comp + 1))

    #Continuum parametres
    parbase = {'value': 0., 'fixed': 0, 'limited': [0, 0], 'limits': [0., 0.], 'tied': '', 'mpmaxstep': 0, 'mpminstep': 0, 'parname': ''}
    if conf['DGCTNUM'] == -1:  # case without continuum
        conf['DGCTNUM'] = 0
        parbase['fixed'] = 1
        contfixed = True
    else:
        contfixed = False
    contind = [i for i in range(len(parinfo), len(parinfo) + conf['DGCTNUM'] + 1)]
    for i in range(conf['DGCTNUM'] + 1):
        parinfo.append(copy.deepcopy(parbase))
        components = np.append(components, -1)
    params = np.zeros(len(parinfo), dtype='>f4')
    for i in range(len(parinfo)):
        params[i] = parinfo[i]['value']
    
    logger.info(' Buildmaps: initial parameters {}'.format(str(params)))

    # Output initialisation to be filled
    paramscube = np.zeros(np.append(len(params), cube.shape[1:]), dtype='>f4')
    perrorcube = np.zeros(np.append(len(params), cube.shape[1:]), dtype='>f4')
    statusmap = np.zeros(cube.shape[1:], dtype='>f4')
    dofmap = np.zeros(cube.shape[1:], dtype='>f4')
    fnormmap = np.zeros(cube.shape[1:], dtype='>f4')
    modcube = np.zeros(cube.shape, dtype='>f4')
    
    #List of extra parameters
    #xxx Ajuster wave et spectrum pour ne garder que ce qui nous intéresse?
    fa = {'wave': wave, 'spectrum': None, 'err': None, 'lines': linewaves, 'psf': conf['SPSF'], 'degc': conf['DGCTNUM'], 'zind': zind, 'sigind': sigind, 'intind': intind, 'ratioind': ratioind, 'contind': contind}

    #Loop on pixels
    counter = 0
    fac_prev = -1
    number_of_pixels = float(cube.shape[2] * cube.shape[1])
    tii = time.time()
    tt = 0
    
    normsp = 10. ** np.floor(np.log10(np.nanstd(cube[indok, :, :])))
    
    ttx0 = time.time()
    for x in range(cube.shape[2]):
        for y in range(cube.shape[1]):
            if int((counter / number_of_pixels) * 100) != fac_prev:
                fac_prev = int((counter / number_of_pixels) * 100)
                sys.stdout.write("Progress => {:3d}%\r".format(fac_prev))
                sys.stdout.flush()
            #if (counter / 10.) == int(counter / 10.): logger.debug('pixel %i / %i'%(counter, number_of_pixels))
            
            counter += 1
            fa['spectrum'] = cube[indok, y, x] / normsp # Spectrum
            fa['err'] = np.sqrt(var[indok, y, x]) / normsp # Error
            #fa['weight'] = np.sqrt(weight[indok, y, x])  # Weight
            
            fa['spectrum'][np.isnan(fa['spectrum'])] = 0
            #fa['err'][np.isnan(fa['err'])] = np.infty
            
            if (np.min(fa['spectrum']) == np.max(fa['spectrum'])) & (np.min(fa['spectrum']) == 0):
                continue  # No need for fitting
            
            minfnorm = np.infty
            #index = None
            
            std = np.std(fa['spectrum'])
            
            #Parameters initialisation
            p = np.copy(params)
            
            #Line intensity initialisation
            for i in intind:
                if parinfo[i]['fixed'] == 0:
                    p[i] = 5 * std * 10 ** (- components[i] + 1.)

            #for i in range(els.index):
                #if parinfo[2 * els.index + i]['fixed'] == 0:  p[2 * els.index + i] = 2 * std

            #Inititalisation of continuum (degree 0)
            if not contfixed:
                p[contind[0]] = np.median(fa['spectrum'])
            #p[els.index * 4] = np.median(fa['spectrum'])
            for z in np.linspace(zzmin, zzmax, ninter + 1):
                #p[comind] = z
                p[zind] = z
                #p[:els.index] = z
                pstart = np.copy(p)
                ti = time.time()
                fit0 = mpfit(myelspectrum, xall=pstart, functkw=fa, quiet=True, parinfo=parinfo, ftol=ftol, xtol=xtol, gtol=gtol, maxiter=maxiter, factor=factor)
                #fit0 = mpfit.mpfit(myelspectrum, xall=pstart, functkw=fa, quiet=True, parinfo=parinfo, ftol=ftol, xtol=xtol, gtol=gtol, maxiter=maxiter, factor=factor)
                tf = time.time()
                tt += tf - ti
                if fit0.status == 0:
                    continue
                if fit0.status == -16:
                    continue
                if fit0.fnorm <= minfnorm:
                    minfnorm = fit0.fnorm
                    fit = copy.copy(fit0)
                #print(z,tf - ti)
            if minfnorm == np.infty:
                continue
            #logger.debug(fit.params[2 * els.index + 1 + 12], parinfo[2 * els.index + 1 + 12]['limits'], fit.params[2 * els.index + 1 + 13])
            ####################################
            # sort of the components based on their flux. In a second pass, we could start from the output parameters and set constraints on e.g. sigma, intensity (e.g. int[comp2] < int[comp1]), etc.)
            # tie intensities to impose one lower than the other?
            if multi_comp > 1:
                ftot = np.zeros(multi_comp)
                for comp in range(multi_comp):
                    ind = (components == (comp + 1))
                    ftot[comp] = (fit.params[np.array(intind)[ind[intind]]] * fit.params[np.array(ratioind)[ind[ratioind]]] * fit.params[np.array(sigind)[ind[sigind]]]).sum()
                sortind = ftot[::-1].argsort() + 1
                paramssort = np.copy(fit.params)
                perrorsort = np.copy(fit.perror)
                for comp in range(multi_comp):
                    paramssort[components == (comp + 1)] = fit.params[components == sortind[comp]]
                    perrorsort[components == (comp + 1)] = fit.perror[components == sortind[comp]]
                fit.params = np.copy(paramssort)
                fit.perror = np.copy(perrorsort)
            paramscube[:, y, x] = fit.params[:]
            perrorcube[:, y, x] = fit.perror[:]
            fnormmap[y, x] = fit.fnorm
            dofmap[y, x] = fit.dof
            statusmap[y, x] = fit.status
            modcube[indok, y, x] = elspectrum(fit.params, wave=wave, lines=linewaves, psf=conf['SPSF'], degc=conf['DGCTNUM'], zind=zind, sigind=sigind, intind=intind, ratioind=ratioind, contind=contind)
        ttx = time.time()
        logger.debug('Execution time column {:}/{:}: {:3.1f}s'.format(x, cube.shape[1], ttx - ttx0))
        ttx0 = ttx
        
    tff = time.time()
    logger.debug('Execution time: {:3.1f}'.format(tff - tii))
    #print(tt)

    #Creating outputs
    modcube *= normsp
    #Error normalisation by reduced chi2
    perrorcube = perrorcube * np.sqrt(fnormmap / dofmap)

    #Continuum
    #if we want the coefficient, we must multiply them by factor + give lambda[0]
    contcube = np.zeros(cube.shape, dtype='>f4')
    econtcube = np.zeros(cube.shape, dtype='>f4')
    for i in range(conf['DGCTNUM'] + 1):
        contcube += paramscube[contind[i], :, :] * (wave0 - wave[0]).reshape(wave0.size, 1, 1) ** i
        econtcube += perrorcube[contind[i], :, :] * (wave0 - wave[0]).reshape(wave0.size, 1, 1) ** i
        #contmap
        #; continuum is computed at the longest wavelength, not necessarily optimal
        #cont[m,n]+=pfinal[2*n_elements(l)+1+i]*double(lambda[n_elements(lambda)-1]-lambda[0])^i/double(lambda[n_elements(lambda)-1]-lambda[0])
        #econt[m,n]+=(paramerr[2*n_elements(l)+1+i]*double(lambda[n_elements(lambda)-1]-lambda[0])^i/double(lambda[n_elements(lambda)-1]-lambda[0]))^2
    contcube *= normsp
    econtcube *= normsp
    
    #Residual cube
    #residual
    residualcube = np.zeros(cube.shape, dtype='>f4')
    residualcube[indok, :, :] = cube[indok, :, :] - modcube[indok, :, :]
    #dispcont
    dispcontmap = np.std(cube[indok, :, :] - modcube[indok, :, :], axis=0)
    
    #Chi2
    chi2map = fnormmap / dofmap
    
    wavemaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    ewavemaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    wavedispmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    ewavedispmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    dispmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    edispmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    intmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    eintmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    fluxmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    efluxmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    snrmaps = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    zmap = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    ezmap = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    rvmap = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    ervmap = np.zeros(np.append(els.index * multi_comp, cube.shape[1:]), dtype='>f4')
    
    for comp in range(multi_comp):
        for i in range(els.index):
            if (not linefit[i]):
                continue
            name = linename[i]
            j = els.lines[els.lines[name].ref].index
            
            #Velocity field
            zmap[comp * els.index + i, :, :] = paramscube[zind[comp * els.index + i], :, :]
            ezmap[comp * els.index + i, :, :] = perrorcube[zind[comp * els.index + i], :, :]
            rvmap[comp * els.index + i, :, :] = compute_rv(zmap[comp * els.index + i, :, :], zcosmo=zcosmo)
            ervmap[comp * els.index + i, :, :] = compute_fwhm(ezmap[comp * els.index + i, :, :], z=zmap[comp * els.index + i, :, :], zcosmo=zcosmo)
            wavemaps[comp * els.index + i, :, :] = (zmap[comp * els.index + i, :, :] + 1) * linewave[i]
            ewavemaps[comp * els.index + i, :, :] = ezmap[comp * els.index + i, :, :] * linewave[i]
            
            #Velocity dispersion
            dzmap = paramscube[sigind[comp * els.index + i], :, :]
            edzmap = perrorcube[sigind[comp * els.index + i], :, :]
            wavedispmaps[comp * els.index + i, :, :] = dzmap * linewave[i]
            ewavedispmaps[comp * els.index + i, :, :] = edzmap * linewave[i]
            dispmaps[comp * els.index + i, :, :] = compute_fwhm(dzmap, z=zmap[comp * els.index + i, :, :], zcosmo=zcosmo)
            edispmaps[comp * els.index + i, :, :] = compute_fwhm(edzmap, z=zmap[comp * els.index + i, :, :], zcosmo=zcosmo)
            
            #Flux
            intmaps[comp * els.index + i, :, :] = paramscube[intind[comp * els.index + i], :, :] * paramscube[ratioind[comp * els.index + i], :, :] * normsp
            fluxmaps[comp * els.index + i, :, :] = np.sum(intmaps[comp * els.index + i, :, :] * np.exp(-0.5 * (wave.reshape(wave.size, 1, 1) - linewave[i] * (1 + zmap[comp * els.index + i, :, :])) ** 2 / ((dzmap * linewave[i]) ** 2 + conf['SPSF'] ** 2)) * sclp * lconvfac, axis=0)
            snrmaps[comp * els.index + i, :, :] = np.sum(weight[firstind0[i]:lastind0[i], :, :] * (modcube[firstind0[i]:lastind0[i], :, :] - contcube[firstind0[i]:lastind0[i], :, :]), axis=0) * sclp * lconvfac / (np.sqrt(np.pi * 2) * np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2) * np.sqrt(np.sum(weight[firstind0[i]:lastind0[i], :, :] * (cube[firstind0[i]:lastind0[i], :, :] - modcube[firstind0[i]:lastind0[i], :, :]) ** 2, axis=0) / np.sum(weight[firstind0[i]:lastind0[i], :, :], axis=0))) * (lastind0[i] - firstind0[i]) / np.sum(weight[firstind0[i]:lastind0[i], :, :], axis=0)
            if free_ratio:
                eintmaps[comp * els.index + i, :, :] = perrorcube[intind[comp * els.index + i], :, :] * normsp
            elif i != j:
                eintmaps[comp * els.index + i, :, :] = np.sqrt((paramscube[intind[comp * els.index + i], :, :] * perrorcube[ratioind[comp * els.index + i], :, :]) ** 2 + (perrorcube[intind[comp * els.index + j], :, :] * paramscube[ratioind[comp * els.index + i], :, :]) ** 2) * normsp
            else:
                eintmaps[comp * els.index + i, :, :] = perrorcube[intind[comp * els.index + i], :, :] * normsp
            
            efluxmaps[comp * els.index + i, :, :] = np.sqrt(np.pi * 2) * np.sqrt((eintmaps[comp * els.index + i, :, :] * np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2)) ** 2 + (linewave[i] * dzmap / np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2) * intmaps[comp * els.index + i, :, :] * edzmap) ** 2)
            
        # This definition suffers inclusion of dz and z, especially when multiple components are fitted
        snrmap = np.sum(weight[indok, :, :] * (modcube[indok, :, :] - contcube[indok, :, :]), axis=0) * sclp * lconvfac * np.size(indok) / (np.sum(weight[indok, :, :], axis=0)) / (np.sqrt(np.pi * 2) * np.sqrt((dzmap * np.median(wave) / (1 + zmap[comind, :, :])) ** 2 + conf['SPSF'] ** 2) * np.sqrt(np.sum(weight[indok, :, :] * (cube[indok, :, :] - modcube[indok, :, :]) ** 2, axis=0) / np.sum(weight[indok, :, :], axis=0)))

        ##Velocity field
        #zmap[comp, :, :] = paramscube[comp * els.index + comind, :, :]
        #ezmap[comp, :, :] = perrorcube[comp * els.index + comind, :, :]
        #rvmap[comp, :, :] = compute_rv(zmap[comp, :, :], zcosmo=zcosmo)
        #ervmap[comp, :, :] = compute_fwhm(ezmap[comp, :, :], z=zmap[comp, :, :], zcosmo=zcosmo)
        
        ##Lines
        
        ## dzmap and edzmap are intermediate products
        #if conf['COMMW']:
            #dzmap = paramscube[(comp + 1) * els.index + comind, :, :]
            #edzmap = perrorcube[(comp + 1) * els.index + comind, :, :]
        
        #for i in range(els.index):
            #if (not linefit[i]): continue
            #name = linename[i]
            #j = els.lines[els.lines[name].ref].index + comp * els.index
            
            ##if (not conf['COMMW']) & (i != j):
                ##dzmap = paramscube[comp * els.index + 1 + j, :, :]
                ##edzmap = perrorcube[comp * els.index + 1 + j, :, :]
            #if (not conf['COMMW']):
                #dzmap = paramscube[(comp + 1) * els.index + j, :, :]
                #edzmap = perrorcube[(comp + 1) * els.index + j, :, :]
            
            #wavemaps[i, :, :] = (zmap + 1) * linewave[i]
            #ewavemaps[i, :, :] = ezmap * linewave[i]
            #wavedispmaps[i, :, :] = dzmap * linewave[i]
            #ewavedispmaps[i, :, :] = edzmap * linewave[i]
            #dispmaps[i, :, :] = compute_fwhm(dzmap, z=zmap, zcosmo=zcosmo)
            #edispmaps[i, :, :] = compute_fwhm(edzmap, z=zmap, zcosmo=zcosmo)
            
            #intmaps[i, :, :] = paramscube[2 * els.index + i, :, :] * paramscube[3 * els.index + i, :, :]
            #fluxmaps[i, :, :] = np.sum(intmaps[i, :, :] * np.exp(-0.5 * (wave.reshape(wave.size, 1, 1) - linewave[i] * (1 + zmap)) ** 2 / ((dzmap * linewave[i]) ** 2 + conf['SPSF'] ** 2)) * sclp * lconvfac, axis=0)
            #snrmaps[i, :, :] = np.sum(weight[firstind0[i]:lastind0[i], :, :] * (modcube[firstind0[i]:lastind0[i], :, :] - contcube[firstind0[i]:lastind0[i], :, :]), axis=0) * sclp * lconvfac / (np.sqrt(np.pi * 2) * np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2) * np.sqrt(np.sum(weight[firstind0[i]:lastind0[i], :, :] * (cube[firstind0[i]:lastind0[i], :, :] - modcube[firstind0[i]:lastind0[i], :, :]) ** 2, axis=0) / np.sum(weight[firstind0[i]:lastind0[i], :, :], axis=0))) * (lastind0[i] - firstind0[i]) / np.sum(weight[firstind0[i]:lastind0[i], :, :], axis=0)
            #if free_ratio:
                #eintmaps[i, :, :] = perrorcube[2 * els.index + i, :, :]
            #elif i != j:
                #eintmaps[i, :, :] = np.sqrt((paramscube[2 * els.index + i, :, :] * perrorcube[3 * els.index + i, :, :]) ** 2 + (perrorcube[2 * els.index + j, :, :] * paramscube[3 * els.index + i, :, :]) ** 2)
            #else:
                #eintmaps[i, :, :] = perrorcube[2 * els.index + i, :, :]
            
            #efluxmaps[i, :, :] = np.sqrt(np.pi * 2) * np.sqrt((eintmaps[i, :, :] * np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2)) ** 2 + (linewave[i] * dzmap / np.sqrt((linewave[i] * dzmap) ** 2 + conf['SPSF'] ** 2) * intmaps[i, :, :] * edzmap) ** 2)
        
        ## XXX Pour les raies individuelles, ne devrait-on pas calculer le flux dans la raie uniquement?
    
        ## XXX ajouter un cas pour lequel on ajoute un niveau seuil a la variance (e.g. sky spectrum pour lequel on veut rajouter un bruit de detecteur, ou si on estime que le niveau bas est trop bas pour que les weights soient realistes)
    
        ## case SNR weighted by inverse of variance / standard deviation (normalised)
        ##snrmap = np.sum(weight[indok, :, :] * (modcube[indok, :, :] - contcube[indok, :, :]), axis = 0) * sclp * lconvfac / (np.sqrt(np.pi * 2) * np.sqrt((dzmap * np.median(wave) / (1 + zmap)) ** 2 + conf['SPSF'] ** 2) * np.sqrt( np.sum(weight[indok, :, :] * (cube[indok, :, :] - modcube[indok, :, :]) ** 2, axis=0) / np.sum(weight[indok, :, :], axis=0)))
    
        ## case SNR weighted by inverse of variance / standard deviation (normalised) + normalised to number of elements
        #snrmap = np.sum(weight[indok, :, :] * (modcube[indok, :, :] - contcube[indok, :, :]), axis = 0) * sclp * lconvfac * np.size(indok) / (np.sum(weight[indok, :, :], axis=0)) /(np.sqrt(np.pi * 2) * np.sqrt((dzmap * np.median(wave) / (1 + zmap)) ** 2 + conf['SPSF'] ** 2) * np.sqrt( np.sum(weight[indok, :, :] * (cube[indok, :, :] - modcube[indok, :, :]) ** 2, axis=0) / np.sum(weight[indok, :, :], axis=0)))
        
        ## case SNR using standard deviation as estimate of noise (might be incorrect after smoothing?)
        ##snrmap = np.sum((modcube[indok, :, :] - contcube[indok, :, :]) / np.sqrt(var[indok, :, :]), axis = 0) * sclp * lconvfac / (np.sqrt(np.pi * 2) * np.sqrt((dzmap * np.median(wave) / (1 + zmap)) ** 2 + conf['SPSF'] ** 2)) #  * np.sqrt( np.sum((cube[indok, :, :] - modcube[indok, :, :]) ** 2 / np.sqrt(var[indok, :, :]), axis=0) / np.sum(var[indok, :, :], axis=0)))
        
        ## case SNR without variance. No weight, noise estimated as the dispersion of the residuals
        ##snrmap = np.sum((modcube[indok, :, :] - contcube[indok, :, :]), axis = 0) * sclp * lconvfac / (np.sqrt(np.pi * 2) * np.sqrt((dzmap * np.median(wave) / (1 + zmap)) ** 2 + conf['SPSF'] ** 2) * np.sqrt( np.sum((cube[indok, :, :] - modcube[indok, :, :]) ** 2, axis=0) / np.size(indok)))
        
    #Output writing
    #Cubes
    hh = writedata(residualcube, hdr, conf['OUTPUT'] + '_residualcube.fits')
    hh = writedata(modcube, hdr, conf['OUTPUT'] + '_modcube.fits')
    hh = writedata(contcube, hdr, conf['OUTPUT'] + '_contcube.fits')
    hh = writedata(econtcube, hdr, conf['OUTPUT'] + '_econtcube.fits')
    
    #Images
    if 'BUNIT' in hdr.keys():
        bunit = hdr['BUNIT']
    else:
        bunit = ''
    if 'CTYPE3' in hdr.keys():
        del hdr['CTYPE3']
    if 'CRVAL3' in hdr.keys():
        del hdr['CRVAL3']
    if 'CRPIX3' in hdr.keys():
        del hdr['CRPIX3']
    if 'CUNIT3' in hdr.keys():
        del hdr['CUNIT3']
    if 'CD3_3' in hdr.keys():
        del hdr['CD3_3']
    if 'CD3_2' in hdr.keys():
        del hdr['CD3_2']
    if 'CD3_1' in hdr.keys():
        del hdr['CD3_1']
    if 'CD2_3' in hdr.keys():
        del hdr['CD2_3']
    if 'CD1_3' in hdr.keys():
        del hdr['CD1_3']
    if 'CDELT3' in hdr.keys():
        del hdr['CDELT3']
        
    # Added by Wilfried to deal with MXDF cubes
    if 'WCSAXES' in hdr:
        
        # If exactly 3 (cube), we change it to 2 (image)
        if hdr['WCSAXES'] == 3:
            hdr['WCSAXES'] = 2
            
        # If < 2 or > 3 then that's weird and we print a warning and we put 2 instead in the images
        elif hdr['WCSAXES'] < 2 or hdr['WCSAXES'] > 3:
            print('WCSAXES keyword in init cube is %d but should be 3. Putting 2 in the images instead.' %hdr['WCSAXES'])
            hdr['WCSAXES'] = 2
            
    
    suff = ''
    if conf['COMMW']:
        suff = '_common'
    else:
        suff = '_indep'
  
    #paramscube
    #perrorcube
    hdr['BUNIT'] = ''
    hh = writedata(dofmap, hdr, conf['OUTPUT'] + '_dof' + suff + '.fits')
    hh = writedata(fnormmap, hdr, conf['OUTPUT'] + '_fnorm' + suff + '.fits')
    hh = writedata(statusmap, hdr, conf['OUTPUT'] + '_status' + suff + '.fits')
    hh = writedata(chi2map, hdr, conf['OUTPUT'] + '_chi2' + suff + '.fits')
    hh = writedata(snrmap, hdr, conf['OUTPUT'] + '_snr' + suff + '.fits')
    hdr['BUNIT'] = bunit
    hh = writedata(dispcontmap, hdr, conf['OUTPUT'] + '_dispcont' + suff + '.fits')
    
    for comp in range(multi_comp):
        if multi_comp == 1:
            suffixe = suff
        else:
            suffixe = suff + '_comp%i' % (comp + 1)
        hdr['BUNIT'] = ''
        hh = writedata(zmap[comp * els.index + comind, :, :], hdr, conf['OUTPUT'] + '_z' + suffixe + '.fits')
        hh = writedata(ezmap[comp * els.index + comind, :, :], hdr, conf['OUTPUT'] + '_ez' + suffixe + '.fits')
        hdr['BUNIT'] = 'km/s'
        hh = writedata(rvmap[comp * els.index + comind, :, :], hdr, conf['OUTPUT'] + '_vel' + suffixe + '.fits')
        hh = writedata(ervmap[comp * els.index + comind, :, :], hdr, conf['OUTPUT'] + '_evel' + suffixe + '.fits')
        
        for i in range(els.index):
            if (not linefit[i]):
                continue
            hdr['BUNIT'] = 'Angstrom'
            hh = writedata(wavemaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_wave' + suffixe + '_' + linename[i] + '.fits')
            hh = writedata(ewavemaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_ewave' + suffixe + '_' + linename[i] + '.fits')
            hh = writedata(wavedispmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_wavedisp' + suffixe + '_' + linename[i] + '.fits')
            hh = writedata(ewavedispmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_ewavedisp' + suffixe + '_' + linename[i] + '.fits')
            hdr['BUNIT'] = bunit
            hh = writedata(intmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_int' + suffixe + '_' + linename[i] + '.fits')
            hh = writedata(eintmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_eint' + suffixe + '_' + linename[i] + '.fits')
            hdr['BUNIT'] = '{} {}'.format(bunit, 'Angstrom')
            hh = writedata(fluxmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_flux' + suffixe + '_' + linename[i] + '.fits')
            hh = writedata(efluxmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_eflux' + suffixe + '_' + linename[i] + '.fits')
            hdr['BUNIT'] = ''
            hh = writedata(snrmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_snr' + suffixe + '_' + linename[i] + '.fits')
            hdr['BUNIT'] = 'km/s'
            if not(conf['COMMW']):
                hh = writedata(dispmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_disp' + suffixe + '_' + linename[i] + '.fits')
                hh = writedata(edispmaps[comp * els.index + i, :, :], hdr, conf['OUTPUT'] + '_edisp' + suffixe + '_' + linename[i] + '.fits')
        if conf['COMMW']:
            hdr['BUNIT'] = 'km/s'
            hh = writedata(dispmaps[comp * els.index + comind, :, :], hdr, conf['OUTPUT'] + '_disp' + suffixe + '.fits')
            hh = writedata(edispmaps[comp * els.index + comind, :, :], hdr, conf['OUTPUT'] + '_edisp' + suffixe + '.fits')
    
    return rvmap


def camel(filename, plot=False, debug=False, free_ratio=False, multi_comp=1, multi_ext=False, config=None, cut=False, clip=False, smooth=False, binned=False, ftol=1e-7, xtol=1e-7, gtol=1e-7, factor=10, maxiter=200):
    """CAMEL (Cube Analysis: Moment maps of Emission Lines) computes moment maps

    Parameters
    ----------
    filename: string
        name of the configuration file
    plot: bool
        keyword to plot fits during the process (not implemented yet)
    debug: bool
        keyword to show debug information (not implemented yet)
    free_ratio: bool
        keyword to allow unconstrained line ratio
    multi_comp: int
        keyword to indicate the number of components to be adjusted
    multi_ext: bool
        keyword to indicate if input fits has multiple extensions
    config: dictionary
        optional dictionary that contains options usually stored in the configuration file

    """
    # XXX gérer message d'erreur si on trouve pas le fichier d'input, s'il n'y a aucune raie, si un paramètre a un problème (e.g. de formatage)...
    # XXX gérer cas avec une LSF donnée en input (non bruitée)
    
    t0 = time.time()
    
    #Reading of configuration file
    logger.info("Reading configuration file " + filename)
    try:
        if filename == '':
            filename = None
        conf = readconfig(filename, config)
    except:
        logger.info("Configuration file problem")
        return
    
    #Reading data
    try:
        logger.info("Reading data cube " + conf['FITSFILE'] + " and variance " + conf['SKYFILE'])
    except:
        logger.info("Reading data cube " + conf['FITSFILE'] + ". No variance data.")
    try:
        cube, hdr, var, varhdr = readcubes(conf, multi_ext=multi_ext)
    except:
        logger.info("Input cube problem")
        return
    
    try:
        testcubes(cube, var)
    except:
        logger.info("Variance size problem")
        #logger.info("Variance shape: %i; Data cube shape: %i"%(np.shape(var), cube.shape))
        return
    
    #XXX when no variance, I should add an option to compute the variance from the cube itself (as done in the IDL code)
    #Cutting the cubes
    logger.info("Cutting data cube")
    cube, hdr = cutcube(cube, hdr, conf)
    hdr = writedata(cube, hdr, conf['OUTPUT'] + '_cube_cut.fits')
    if var is not None:
        logger.info("Cutting variance")
        var, varhdr = cutcube(var, varhdr, conf)
        if var.ndim == 3:
            var = remove_nanvar(var)
        varhdr = writedata(var, varhdr, conf['OUTPUT'] + '_var_cut.fits')
    wlih = muse_whitelight_image(cube, hdr, conf['OUTPUT'] + '_cut_whitelight.fits')
    if cut & (not(clip)) & (not(smooth)):
        return
    
    #Clipping the cube
    if (conf['SCLIP'] != 0) & (conf['SCLIP'] is not None):
        logger.info('Performing {:3.1f}-sigma clipping using box of size {:d}. Number of pass: {:d}'.format(conf['SCLIP'], conf['XYCLIP'], conf['NCLIP']))
        cube, hdr = clipcube(cube, hdr, xy=conf['XYCLIP'], sclip=conf['SCLIP'], npass=conf['NCLIP'])
        hdr = writedata(cube, hdr, conf['OUTPUT'] + '_cube_cut_clip.fits')
    wlih = muse_whitelight_image(cube, hdr, conf['OUTPUT'] + '_cut_clip_whitelight.fits')
    if clip & (not(smooth)):
        return
    
    #Smoothing the cubes
    if (conf['WSMOOTH'] != 0) & (conf['WSMOOTH'] is not None):
        logger.info('Performing {:3.1f} pixels 1D spectral smoothing'.format(conf['WSMOOTH']))
        cube, hdr = spectralsmooth(cube, hdr, conf['WSMOOTH'])
        conf['OUTPUT'] = conf['OUTPUT'] + '_wsmooth'
        hdr = writedata(cube, hdr, conf['OUTPUT'] + '_cube.fits')
        if var is not None:
            if var.ndim == 3:
                #var, varhdr = spectralsmooth(var, varhdr, conf['WSMOOTH'])
                var, varhdr = var_spectralsmooth(var, varhdr, conf['WSMOOTH'])
                ## variance is divided by the integral of the kernel to account for the variance lowering compared to the signal when smoothed
                #var /= ((conf['WSMOOTH'] / (2. * np.sqrt(2. * np.log(2.)))) * np.sqrt(2 * np.pi))
                varhdr = writedata(var, varhdr, conf['OUTPUT'] + '_var.fits')
        
    if (conf['SSMOOTH'] != 0) & (conf['SSMOOTH'] is not None):
        logger.info('Performing {:3.1f} pixels 2D gaussian spatial smoothing'.format(conf['SSMOOTH']))
        cube, hdr = spatialsmooth(cube, hdr, conf['SSMOOTH'])
        conf['OUTPUT'] = conf['OUTPUT'] + '_ssmooth'
        hdr = writedata(cube, hdr, conf['OUTPUT'] + '_cube.fits')
        if var is not None:
            if var.ndim == 3:
                #var, varhdr = spatialsmooth(var, varhdr, conf['SSMOOTH'])
                var, varhdr = var_spatialsmooth(var, varhdr, conf['SSMOOTH'])
                ## variance is divided by the integral of the kernel to account for the variance lowering compared to the signal when smoothed
                #var /= ((conf['SSMOOTH'] / (2. * np.sqrt(2. * np.log(2.)))) ** 2 * 2 * np.pi)
                varhdr = writedata(var, varhdr, conf['OUTPUT'] + '_var.fits')
    wlih = muse_whitelight_image(cube, hdr, conf['OUTPUT'] + '_whitelight.fits')
    if smooth:
        return
    
    if (conf['BINNING'] != 0) & (conf['BINNING'] is not None):
        logger.info(f"Performing {conf['BINNING']} pixels spatial binning")
        cube, hdr = spatialbin(cube, hdr, conf['BINNING'])
        conf['OUTPUT'] = conf['OUTPUT'] + '_binned'
        hdr = writedata(cube, hdr, conf['OUTPUT'] + '_cube.fits')
        if var is not None:
            if var.ndim == 3:
                var, varhdr = var_spatialbin(var, varhdr, conf['BINNING'])
                ## variance is divided by the integral of the kernel to account for the variance lowering compared to the signal when smoothed
                #var /= ((conf['SSMOOTH'] / (2. * np.sqrt(2. * np.log(2.)))) ** 2 * 2 * np.pi)
                varhdr = writedata(var, varhdr, conf['OUTPUT'] + '_var.fits')
    wlih = muse_whitelight_image(cube, hdr, conf['OUTPUT'] + '_whitelight.fits')
    if binned:
        return
    
    #Creating the maps
    rv = buildmaps(conf, cube, hdr, var=var, plot=plot, debug=debug, free_ratio=free_ratio, multi_comp=multi_comp, ftol=ftol, xtol=xtol, gtol=gtol, factor=factor, maxiter=maxiter)

    t1 = time.time()
    #print(t1-t0)
    
    return


#def usage(option, opt, value, parser):
def usage():
    """CAMEL (Cube Analysis: Moment maps of Emission Lines) can by launched from command line
    or from a python console.
    
    #########################
    #Python console solution#
    #########################
    
    Syntaxe:
    --------
    import camel
    camel(filename, plot=False, debug=False, free_ratio=False, multi_ext=False, config=None)
    
    Parameters
    ----------
    filename: string
        name of the configuration file
    plot: bool
        keyword to plot fits during the process (not implemented yet)
    debug: bool
        keyword to show debug information (not implemented yet)
    free_ratio: bool
        keyword to allow unconstrained line ratio
    multi_ext: bool
        keyword to indicate if input fits has multiple extensions
    config: dictionary
        optional dictionary that contains options usually stored in the configuration file

    
    #######################
    #Command line solution#
    #######################
    
    Syntaxe:
    --------
    
    """
    print(usage.__doc__)


def main(argv):
    """
    """
    parser = argparse.ArgumentParser(description=usage())
    parser.add_argument('--file', '-f', action="store", dest="filename", default='', help="name of the configuration file")
    parser.add_argument('--plot', '-p', action="store_true", dest="plot", default=False, help="keyword to plot fits during the process (not implemented yet)")
    parser.add_argument('--debug', '-d', action="store_true", dest="debug", default=False, help="keyword to show debug information (not implemented yet)")
    parser.add_argument('--free_line_ratio', '-r', action="store_true", dest="free_ratio", default=False, help="keyword to allow unconstrained line ratio")
    parser.add_argument('--multi_comp', '-c', action="store", dest="multi_comp", default=1, type=int, help="Number of components")
    parser.add_argument('--multi_ext', '-m', action="store_true", dest="multi_ext", default=False, help="keyword to indicate if input fits has multiple extensions")
    parser.add_argument('--ftol', action="store", dest="ftol", default=1e-7, type=float, help="ftol argument in mpfit")
    parser.add_argument('--xtol', action="store", dest="xtol", default=1e-7, type=float, help="xtol argument in mpfit")
    parser.add_argument('--gtol', action="store", dest="gtol", default=1e-7, type=float, help="gtol argument in mpfit")
    parser.add_argument('--maxiter', action="store", dest="maxiter", default=200, type=int, help="maxiter argument in mpfit")
    parser.add_argument('--factor', action="store", dest="factor", default=10, type=float, help="factor argument in mpfit (between 0.1 and 100)")
    parser.add_argument('--FITSFILE', action="store", dest="FITSFILE", default=None, help="name of the input cube")
    parser.add_argument('--OUTPUT', action="store", dest="OUTPUT", default=None, help="generic output name")
    parser.add_argument('--SKYFILE', action="store", dest="SKYFILE", default=None, help="name of the variance cube (can be a sky cube)")
    parser.add_argument('--HALPHA', action="store_true", dest="HALPHA", default=None, help="keyword to fit Halpha@6562.801 line")
    parser.add_argument('--HBETA', action="store_true", dest="HBETA", default=None, help="keyword to fit Hbeta@4861.363 line")
    parser.add_argument('--HGAMMA', action="store_true", dest="HGAMMA", default=None, help="keyword to fit Hgamma@4340.47 line")
    parser.add_argument('--HDELTA', action="store_true", dest="HDELTA", default=None, help="keyword to fit Hdelta@4101.73 line")
    parser.add_argument('--HEPS', action="store_true", dest="HEPS", default=None, help="keyword to fit Hepsilon@3970.07 line")
    parser.add_argument('--NII6548', action="store_true", dest="NII6548", default=None, help="keyword to fit NII@6548.05 line")
    parser.add_argument('--NII6583', action="store_true", dest="NII6583", default=None, help="keyword to fit NII@6583.45 line")
    parser.add_argument('--SII6716', action="store_true", dest="SII6716", default=None, help="keyword to fit SII@6716.44 line")
    parser.add_argument('--SII6731', action="store_true", dest="SII6731", default=None, help="keyword to fit SII@6730.82 line")
    parser.add_argument('--OIII4363', action="store_true", dest="OIII4363", default=None, help="keyword to fit OIII@4363 line")
    parser.add_argument('--OIII4959', action="store_true", dest="OIII4959", default=None, help="keyword to fit OIII@4958.911 line")
    parser.add_argument('--OIII5007', action="store_true", dest="OIII5007", default=None, help="keyword to fit OIII@5006.843 line")
    parser.add_argument('--OII', action="store_true", dest="OII3729", default=None, help="keyword to fit OII doublet @3726.04,3728.80")  # ['OII3729','OII3726']
    parser.add_argument('--OI6300', action="store_true", dest="OI6300", default=None, help="keyword to fit OI@6300 line")
    parser.add_argument('--HEI4471', action="store_true", dest="HeI4471", default=None, help="keyword to fit HeI@4471 line")
    parser.add_argument('--HEI5876', action="store_true", dest="HeI5876", default=None, help="keyword to fit HeI@5876 line")
    parser.add_argument('--HEII4686', action="store_true", dest="HeII4686", default=None, help="keyword to fit HeII@4686 line")
    parser.add_argument('--NEIII3868', action="store_true", dest="NeIII3868", default=None, help="keyword to fit NeIII@3868 line")
    #parser.add_argument('--EXTRAL', action="append", dest="EXTRAL", default=None, type=float, help="keyword to add a new line (wavelength in angstrom) - multiple extra wavelengths can be added")
    parser.add_argument('--EXTRAL', action="append", nargs='*', default=None, help="keyword to add a new line. Can be provided, in this order: wavelength in Angstroms, reference line, theoretical, lower and upper ratios - multiple extra wavelengths can be added")
    parser.add_argument('--COMMW', action="store_true", dest="COMMW", default=None, help="keyword to fix common widths (in velocity) between several fitted lines")
    parser.add_argument('--REDSHIFT', action="store", dest="REDSHIFT", default=None, type=float, help="mean redshift of the object")
    parser.add_argument('--REDMIN', action="store", dest="REDMIN", default=None, type=float, help="minimum redshift allowed for fitting")
    parser.add_argument('--REDMAX', action="store", dest="REDMAX", default=None, type=float, help="maximum redshift allowed for fitting")
    parser.add_argument('--ZRMIN', action="store", dest="ZRMIN", default=None, type=float, help="minimum redshift for spectral range considered around lines")
    parser.add_argument('--ZRMAX', action="store", dest="ZRMAX", default=None, type=float, help="maximum redshift for spectral range considered around lines")
    parser.add_argument('--INITW', action="store", dest="INITW", default=None, type=float, help="initial line width in km/s")
    parser.add_argument('--WMIN', action="store", dest="WMIN", default=None, type=float, help="minimum width allowed for fitting in km/s")
    parser.add_argument('--WMAX', action="store", dest="WMAX", default=None, type=float, help="maximum width allowed for fitting in km/s")
    parser.add_argument('--DFIT', action="store", dest="DFIT", default=None, type=float, help="bin in km/s for searching the position of the lines")
    parser.add_argument('--DGCTNUM', action="store", dest="DGCTNUM", default=None, type=int, help="continuum degree")
    parser.add_argument('--SCLIP', action="store", dest="SCLIP", default=None, type=float, help="sigma-clipping threshold to clean the cube")
    parser.add_argument('--XYCLIP', action="store", dest="XYCLIP", default=None, type=int, help="width of the box to compute the median for the sigma-clipping")
    parser.add_argument('--NCLIP', action="store", dest="NCLIP", default=None, type=int, help="number of pass for clipping")
    parser.add_argument('--LSF', action="store", dest="SPSF", default=None, type=float, help="spectral PSF width in angstroms (dispersion of the Gaussian)")
    parser.add_argument('--WSMOOTH', action="store", dest="WSMOOTH", default=None, type=float, help="width for Gaussian spectral smoothing in pixel")  # options? gaussian uniquement?
    parser.add_argument('--SSMOOTH', action="store", dest="SSMOOTH", default=None, type=float, help="width for Gaussian spatial smoothing in pixel")
    parser.add_argument('--BINNING', action="store", dest="BINNING", default=None, type=int, help="Size of the binning box")
    parser.add_argument('--XMIN', action="store", dest="XMIN", default=None, type=int, help="lower cut window in x direction of the cube in pixel")
    parser.add_argument('--YMIN', action="store", dest="YMIN", default=None, type=int, help="upper cut window in x direction of the cube in pixel")
    parser.add_argument('--ZMIN', action="store", dest="ZMIN", default=None, type=int, help="lower cut window in y direction of the cube in pixel")
    parser.add_argument('--XMAX', action="store", dest="XMAX", default=None, type=int, help="upper cut window in y direction of the cube in pixel")
    parser.add_argument('--YMAX', action="store", dest="YMAX", default=None, type=int, help="lower cut window in z direction of the cube in pixel")
    parser.add_argument('--ZMAX', action="store", dest="ZMAX", default=None, type=int, help="upper cut window in z direction of the cube in pixel")
    parser.add_argument('--cut', action="store_true", dest="cut", default=False, help="keyword to stop after cutting the cube")
    parser.add_argument('--clip', action="store_true", dest="clip", default=False, help="keyword to stop after clipping the cube")
    parser.add_argument('--smooth', action="store_true", dest="smooth", default=False, help="keyword to stop after smoothing the cube")
    parser.add_argument('--bin', action="store_true", dest="binned", default=False, help="keyword to stop after binning the cube")

    #parser.add_argument('--MFIT', action="store", dest="MFIT", default=None)
    #parser.add_argument('--THRES', action="store", dest="THRES", default=None, type=float)
    #parser.add_argument('--MEDIAN', action="store", dest="MEDIAN", default=None, type=float)
    #parser.add_argument('--FITSPSF', action="store", dest="FITSPSF", default=None, type=float)
    
    try:
        options = parser.parse_args()
    except:
        sys.exit(2)
    
    if np.size(argv) < 1:
        sys.exit(2)
    
    opts = vars(options)   # this is a dictionary
    opts['OII3726'] = opts['OII3729']
    camel(options.filename, plot=options.plot, debug=options.debug, free_ratio=options.free_ratio, multi_comp=options.multi_comp, multi_ext=options.multi_ext, ftol=options.ftol, xtol=options.xtol, gtol=options.gtol, maxiter=options.maxiter, factor=options.factor, config=opts, cut=options.cut, clip=options.clip, smooth=options.smooth, binned=options.binned)
    return

if __name__ == "__main__":
    main(sys.argv[1:])
