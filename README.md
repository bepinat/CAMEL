# CAMEL - Cube Analysis: Moment maps of Emission Lines

### What is it?
**CAMEL** is a tool that enables to derive kinematics maps by fitting emission lines with Gaussians on data cubes. It has been developped from an IDL code written in the frame of MASSIV and MUSE first used in the following analyses:

- [Epinat, B. et al. (2009)](http://cdsads.u-strasbg.fr/abs/2009A%26A...504..789E)

- [Epinat, B. et al. (2012)](http://cdsads.u-strasbg.fr/abs/2012A%26A...539A..92E)

The package also contains a tool dedicated to adjust 1D spectra called **SAMEL (Spectrum Analysis: Moment maps of Emission Lines)** and a tool to generated configuration files (create_config.py).

### Requirements
It runs with python above version 2.7 (including versions 3.x) and uses standard python libraries:
time, os, sys, copy, numpy, pyfits, scipy, optparse

### Methods
CAMEL is based on the Levendberg-Marquart minimisation technique and uses the **mpfit** library that can be found [here](https://code.google.com/p/astrolibpy/downloads/detail?name=mpfit.tar.gz&can=2&q=).
To work both with python 2.7 and python 3.x, we have used the [cap_mpfit.py](http://www-astro.physics.ox.ac.uk/~mxc/software/) version of **mpfit** provided by Michele Capellarri.

To be executable from command line, it has to be in the path environment variable.
